---
date: 2018-11
modified: 2019-01-05
title: Retard de paiement
...

# Retard de paiement

Cher CROUS,

J’ai bien reçu vos lettres des 20 avril, 21 septembre, 
24 octobre, 20 novembre, 19 décembre 2018 et 16 janvier 2019. 
J’ai pris note de la dette que j’ai contractée à votre égard, 
dans le cadre de l’augmentation du loyer de la résidence entre 
décembre 2017 et janvier 2018 — dette qui pour mémoire s’élève 
à la somme de 1,20 €. J’ai conscience que le prélèvement 
automatique de mon loyer ne saurait me servir d’excuse. Je 
comprends bien que vous envisagiez de traiter ce contentieux par 
huissier.

Hélas, étant actuellement sans revenu, il m’est difficile de 
réunir la somme demandée dans son intégralité ; aussi vous 
suis-je reconnaissant de ne pas avoir exercé l’autorisation de 
prélèvement bancaire que je vous avais inconsidérément signée. 
Je vous écris donc dans l’espoir que nous puissions établir un 
arrangement à l’amiable. Serait-il possible d’échelonner le 
recouvrement de ladite dette, par exemple suivant un échéancier 
en douze mensualités de 0,10 € ? Je serais disposé à régler les 
paiments par chèque le 30 de chaque mois. En gage de ma bonne 
foi, je suis prêt à déposer en caution à l’accueil, une baguette 
moulée d’une valeur de 1,20 €.

Restant ouvert à d’autres options et dans l’attente de votre 
réponse, je vous prie d’agréer, Directrice, Directeur, 
l’expression de mes salutations distinguées.

Monsieur.

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
