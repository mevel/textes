---
date: 2021-04-21
modified: 2021-05-17
summary: atelier d’écriture, thème « L’enquête »
...

# L’étrange disparition d’Agatha Pinson

Pour les journaux, l’histoire commença à la rentrée de janvier. 
À la police, David dirait qu’elle avait commencé deux semaines 
plus tôt, dès le début des vacances de Noël. Il garderait pour 
lui ce qu’on ne lui demanderait pas : pour ce que lui-même en 
savait, cette histoire avait en fait commencé près de trente ans 
plus tôt. Il ne pouvait pas voir au-delà, mais peut-être 
était-elle bien plus ancienne encore. Il était possible que 
l’événement qui défraya la chronique cette année-là n’était que 
le dernier développement d’une histoire dont l’origine remontait 
à un demi-siècle. Peut-être même traversait-elle les 
générations ?

Toujours est-il que le lundi 5 janvier, les jeunes élèves de 
Mademoiselle Pinson attendirent en vain leur maîtresse.

À 9 H 15, la directrice envoya les 24 enfants — qui ne pouvaient 
plus rester calmes dans le froid mordant de la cour — dans la 
bibliothèque de l’école, puis téléphona au domicile de 
Mademoiselle Pinson. Celle-ci ne répondit pas. « Peut-être 
a-t-elle eu un accident », se dit Madame Ravine, « et est-elle 
partie à l’hôpital, ou un autre imprévu de la sorte. » La 
directrice donna des devoirs aux enfants pour les occuper 
jusqu’à l’heure de la cantine. Après déjeuner, comme elle 
n’avait toujours pas de nouvelle de leur maîtresse, elle fut 
forcée de les renvoyer dans leurs foyers.

Après l’école, Madame Ravine n’avait toujours pas l’ombre d’une 
explication. Elle chercha qui appeler avant de réaliser qu’elle 
ne connaissait pas les proches de Mademoiselle Pinson. Au volant 
de sa petite voiture (petite, pour les goûts locaux seulement), 
elle se rendit au domicile de sa maîtresse de première année, 
à l’écart de la ville, en lisière de la forêt. C’était la 
première fois que Madame Ravine venait ici : elle découvrit une 
vieille bicoque en pin passablement délabrée. Personne ne vint 
lui ouvrir. Elle tenta même d’actionner la poignée, mais la 
porte était verrouillée. Elle fit le tour de la maison en se 
frictionnant les mains : aucune lumière ne sortait des fenêtres 
alors que la nuit tombait.

Elle se reporta sur la maison voisine (voisine, selon les 
critères locaux seulement), en se rappelant que c’était Monsieur 
Périllon qui vivait là.

— Bonsoir David, dit Madame Ravine. Agatha n’est pas venue 
  à l’école aujourd’hui, sais-tu où elle est ?

Monsieur Périllon répondit que non, il ne savait pas où elle 
était, et qu’il ne l’avait pas vue depuis « quelques jours », 
mais qu’ils échangeaient rarement et n’était pas dans ses 
secrets.

— On dirait qu’elle n’est pas là, et je n’arrive pas à la 
  joindre. Je suis un peu inquiète. Qui je pourrais contacter ?

David la regarda bizarrement :

— Personne. Elle n’a aucune famille.

La directrice de l’école fut interloquée. Aucune famille ? 
Comment c’était possible ? Mais après tout, maintenant qu’elle 
y songeait, c’est vrai qu’elle ne l’avait jamais vue en 
compagnie de qui que ce soit.

À l’abri dans sa voiture, elle appela l’hôpital : le médecin de 
garde lui assura qu’Agatha ne s’y trouvait pas. Elle alerta 
enfin la police communale, à 19 H 42. Ce fut l’agent Coty, un 
jeunot, qui décrocha. Le transcript de cet échange téléphonique 
serait consigné dans le dossier d’enquête.

— Bonsoir, police de XXX, que puis-je pour vous ?
— Ah, bonsoir Julien. Je suis très inquiète, Agatha est 
  introuvable et…
— Qui ça ?
— Mais… tu sais, Agatha Pinson, l’institutrice à l’école…
— Ah, hmm, oui. Bon, décris-moi la situation.

Alors comme ça Julien ne connaissait pas Agatha ? Il allait 
devoir faire un peu plus attention aux habitants de sa 
juridiction. Madame Ravine était certaine que Georges, le vieux 
{shérif}, connaissait tout le monde. Après les explications, le 
bleu convint que c’était suspect.

— Bon, Madame Ravine, voilà ce qu’on va faire. Je vais signaler 
  la disparition au niveau de l’État. Si elle ne réapparait pas 
  demain, passez au poste pour qu’on enregistre votre 
  témoignage.

Madame Ravine raccrocha. La voix du jeune policier avait semblé 
presque enjouée, comme s’il se réjouissait d’avoir enfin un peu 
d’action.

XXX

— Aucune famille ? Madame Ravine, elle enseigne dans votre école 
  depuis 30 ans et vous ne savez rien sur elle ? Comment c’est 
  possible ?
— Elle est… très discrète.

<!--

  Madame Ravine
  Monsieur Périllon
  Monsieur Coty

  c’était en fait ses élèves qui la connaissaient le mieux
  portrait-robot
  aucune photo: sur les photos de classe, toujours hors cadre ou 
  remplacée par quelqu’un d’autre
  pas d’apparition dans le journal
  aucune Agatha Pinson enregistrée dans l’état-civil de XXX, ni 
  d’aucun autre état.
  la police finit par conclure, fort logiquement, qu’elle 
  n’existait pas.
  et qu’il n’y avait donc pas d’affaire, personne à chercher
  l’intérêt médiatique retomba bien vite
  invention de David
  qui écoute des enfants ?
  amants
  solitaire ou pas ?
  petite maison isolée
  chute: David sort une photo volée d’eux deux

  à son arrivée: un accent net qu’elle s’efforce de dissimuler
  David fait partie des rares qui peuvent s’en souvenir

-->

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
