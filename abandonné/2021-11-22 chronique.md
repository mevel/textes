---
date: 2021-11-22
modified: 2022-01-10
summary: atelier d’écriture,
    thème « La chronique racontera » (22/11)
    puis « La cérémonie » (10/01)
...

# La chronique racontera

## 1964

Quand l’Oromestre embrassa le tarmac impie, filmé par toutes les 
caméras du Septentrion, chacun comprit que Jeb reprenait sous 
son manteau ces vastes terres qu’on croyait à jamais damnées.

— Cette terre est sauvée, car Jeb y règne à nouveau.

Alors la liesse éclata dans toutes les rues de la civilisation. 
Aux clameurs de « Jeb pardonne ! Jeb est grand ! » se mêlaient 
les confettis et les petits fanions. La joie était d’autant plus 
sauvage que la guerre avait été longue, totale et meurtrière… et 
qu’elle n’était pas la première. Ça n’était rien moins que deux 
modèles du monde qui s’étaient affrontés dans une lutte 
existentielle.

Le fléau plongeait ses racines dans les siècles passés. Quand, 
au 18e siècle après Ahmès-Sisme, un obscur théologien de Palerme 
(salée soit cette ville corruptrice !) avait suggéré que la 
colère de Jeb fût « épreuve plutôt que châtiment » (écartelé vif 
soit l’esprit malade capable même de concevoir telle injure à la 
raison !), quand il avait élucubré cela, personne n’avait prêté 
l’oreille. Il faut dire, il est vrai, que la Grande Colère de 
Lisbonne venait d’ébranler le monde et qu’on entendait alors 
toutes sortes de folies. Hélas, le dément avait profité de cette 
inattention (ébouillanté soit ce fourbe entre les fourbes !), 
avait persisté, imprimé des livres (consumés dans les flammes 
purificatrices soient ces écrits impies !) et fait des 
disciples. Avec ces gens à la nature intranquille, les querelles 
s’étaient multipliées mais, avant que l’Église eût pu réaliser 
le systématisme du motif, l’« éprouvisme » (maudite soit sa 
thèse absurde !), l’éprouvisme avait contaminé toutes les 
grandes villes du Septentrion. Les voisins contre leurs voisins, 
les sœurs contre leurs frères et les fils contre leurs mères, la 
discorde s’était emparée des cœurs.

C’est en 1780 que, constatant enfin l’ampleur du problème, Sa 
Sainteté Antef VI avait déclaré ces croyances hérétiques — ce 
qu’elles étaient en réalité. Beaucoup d’hommes justes qui 
avaient été égarés virent alors la raison. Toutefois, des âmes 
inexpiables se renfrognèrent dans leur erreur. Ils contestèrent 
toujours plus de principes et de coutumes. Certains, les plus 
insensés, invitaient même leurs semblables à se lever du pied 
gauche ! Que l’Oromestre condamnât les nouvelles coutumes, 
ceux-ci en créaient d’autres encore. Qu’il mît à l’Index les 
livres des éprouvites, ceux-là criaient à la tyrannie. Ce fut le 
début de disputes violentes. La foi se mêla de politique. Les 
successions firent des guerres où la confession des prétendants 
comptait autant que leur ascendant. De batailles en émeutes, 
d’assassinats en pogroms, le Septentrion était à feu et à sang, 
sans que ce soit le fait de Jeb. Certaines couronnes tombèrent 
entre des mains éprouvites. Des peuples migraient.

Il fallut un demi-siècle de violences épisodiques pour que la 
situation se stabilisât en Europe. Les principaux royaumes étant 
revenus dans la foi, nombreux furent les éprouvites qui prirent 
l’océan, au-delà du sable d’Occident, pour le Nouveau Monde. Ce 
continent donné par Jeb était vierge, ils en firent des pays 
selon leurs idées perverties. Cependant, débarrassée des plus 
farouches fanatiques, la paix régnait à nouveau en terre 
nihilite.

Le temps passa. L’Amérique entra dans l’Histoire. Elle fut même 
un temps l’alliée des nihilites. Ramollies par les idées 
modernes, les mœurs de l’époque étaient volontiers tolérantes.

Ce laxisme ne dura pas : une crise économique terrible suscita 
un sursaut de ferveur. Quand le pieux Septentrion souffrait de 
la misère, les Américains sacrilèges s’ébattaient dans une 
opulence indécente, que seul leur dévoiement permettait. Après 
la provocation de trop, l’Oromestre Urbain III prononça la 
Guerre sainte en 1938. Le mal devait être éradiqué. Les deux 
blocs jetèrent toutes leurs forces dans le combat qui devait 
sonner le glas pour l’un deux. L’issue fut longtemps incertaine. 
Plusieurs fois le sort sembla se retourner. Les desseins de Jeb 
empruntent des galeries secrètes. L’engagement fit rage sur 
trois décennies et décima plus d’une génération.

Mais enfin l’Oromestre baisait le sol de New-York, les 
éprouvites étaient définitivement terrassés, et leur continent 
souillé rendu au sain nihilisme.

## 1755

_À suivre…_

<!--------------------------------------------------------------

https://fr.wikipedia.org/wiki/Noms_et_prénoms_égyptiens_dans_l'Antiquité_(A_à_G)

en Égypte ancienne…

−3012: petit séisme -> une brique se détache d’un muret et tombe 
sur le pied d’un paysan -> consigné par un scribe

−3011: le seigneur local exagère les dégâts (village détruit, 
4 morts) pour recevoir plus d’argent du pouvoir central 
(Pharaon)

−2972: un jeune scribe archiviste interprète mal le hiéroglyphe 
« village » et écrit « ville »

−2623: un archiviste voit la contradiction entre toute une 
« ville » détruite et le faible nombre de victimes; il procède 
à une recherche historique pour déterminer la population de la 
ville (laquelle? plusieurs identification possibles vu que le 
nom du village ne correspond à aucune ville réelle) à l’époque, 
et suppose que la moitié de la population est morte

XXX: un historien grec (impressionné par les marées en 
Mer-Rouge, vu que c’est un phénomène inconnu en Méditerranée) 
ajoute que, vu l’ampleur du séisme, il a sans nul doute 
déclenché un tsunami qui a balayé tout le pays

XXX: "an 1" : le Sacrifice d’Ahmès-Sisme (prophète)
qui prétend empêcher les séismes (« où est Son pied, Jeb ne 
tremble point / reste calme car Jeb ne voulût point que ce pied 
trembla »)
envoyé divin, seul détenteur d’un tel pouvoir, toute suggestion 
contraire étant hérésie

1755? séisme de Lisbonne

religion: le Guébianisme, les guébiens
  adoration du dieu Geb (déformé au cours des siècles en Gué ou 
  Jeb)
  séparée en 2 branches irréconciliables :
    - le nihilisme (les nihilites)
        croient que quand Geb envoie des séismes, c’est qu’il 
        veut anéantir l’humanité et qu’ils doivent accepter 
        docilement la fatalité
    - l’éprouvisme (les éprouvites)
        croient que Geb teste l’humanité en éprouvant sa 
        capacité à survivre

Geb = dieu égyptien de, frère et amant de Nout déesse du ciel
~= Gaïa (déesse grecque)

ville impériale = Alexandrie
ville sainte = l’obscur village

https://fr.wikipedia.org/wiki/Mer_Méditerranée#Toponymie
nom égyptien ancien = « la Grande Verte »
grec = Mesogeios thalassa = « Mer au milieu des terres »
ou « mer située en deçà des Colonnes d'Hercule »

-->

# vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—:
