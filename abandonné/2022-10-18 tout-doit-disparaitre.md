---
date: 2022-10-11
finished: 2022-10-11
modified: 2022-10-18
summary: atelier d’écriture,
    thème « tout doit disparaître » ;
    inspiré par un bout de papier qui traînait sur la table et 
    qui traitait de la censure au cinéma, en montrant des 
    critiques qui dénonçaient la violence gratuite de films 
    ("Silent Assassins" et "High on Life")
title: Tout doit disparaître
...

# Tout doit disparaître / La fin des producteurs

Ce matin-là, Jonas débordait d’une joie irrépressible. En se 
redressant, il a ri comme il n’avait plus ri depuis ses 10 ans. 
Avant de sortir, il a même fait la bise à sa mère, qui avait les 
joues froides. Dehors le Soleil resplendissait. Le déluge de la 
nuit avait tout balayé bien propre et la rue, les trottoirs, les 
devantures luisaient de rosée. Les passants le saluaient. Des 
moineaux chantaient quand il a traversé le parc. Des enfants 
criaient sur les toboggans. Il s’est arrêté à la boulangerie 
pour acheter un beignet, un bon beignet à la pomme tout chaud, 
tout juste sorti du four, qui lui rappelait son enfance. Il 
était attablé dans un coin du commerce quand une jolie brune est 
entrée. Il lui a souri. Elle l’a regardé en retour. La journée 
s’annonçait magnifique. Musique douce, pano sur le Soleil 
radieux, lent fondu au blanc.

Bien sûr, certains détails superflus ont été omis.

Par exemple, inutile de montrer l’expression de la boulangère. 
Ou le visage de la brune. Laissons donc l’imagination travailler 
— n’oublions pas la mort de l’auteur ! On garde le début du plan 
suivant — la fille de dos, des fesses bien moulées, ça c’est 
bon —, on coupe juste avant qu’elle tourne les talons tout net. 
Trop univoque. Ça ferme les possibles quand il serait bienvenu 
de les ouvrir. On veut laisser un bon feeling.

Bon, Jonas est complètement barbouillé, on met un filtre, c’est 
dégueu mais c’est obligatoire.

Les chiens qui hurlent devant Jonas au parc, on coupe. Bruyant, 
angoissant, aucun intérêt. On a des phobies dans le public.

L’interminable gros plan sur ses mains qui dégouttent, pendant 
qu’il marche. C’est trop bizarre. On ne comprend pas. On coupe.

On se dispense de mentionner le sans-abri que Jonas croise dans 
la rue. C’est sale, un sans-abri, c’est laid, c’est une vérité 
inconfortable pour l’Amérique. La critique sociale sous-jacente 
est facile et franchement malvenue. C’est une image biaisée. 
L’Amérique est la terre des possibles, et le scénariste dans son 
loft hors de prix en Californie devrait bien le savoir. Et de 
toute façon Jonas le croise tous les jours. Bref, on coupe.

En plus il est crevé, le sans-abri, vraiment, à quoi ça rime, on 
ne veut pas montrer ça. Les stups vont devoir faire une petite 
descente en Californie. Mais regardez-moi ça, les tripes 
à l’air, le sang giclé sur les murs, on nage en plein délire.


Cette scène de violence est absolument gratuite et n’apporte 
rien au propos.

En réalité, 

- boulangère
- sans-abri
- chiens
- cris enfants

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
