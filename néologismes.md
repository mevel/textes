---
date: 2013
title: Néologismes
...

# Néologismes

- postension
- postention
- malpathoque
- patatronique
- colimard
- galimaçon
- graspon
- bourredail
- vromisson
- pardensus
- maugluchade (© J.N.)
- dépalguer
- galpophile
- glycophrène
- confixe
- condolescence
- surmission
- sofranoux
- cascaban
- lampignon
- sitigieux
- posticipation
- rétrocipation
- reculonnade
- cahu-cahot

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
