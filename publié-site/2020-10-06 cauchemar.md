---
date: 2020-10-06
modified: 2023-10-28
summary: atelier d’écriture,
    réécriture de la nouvelle <i>Un rêve</i> (1831) 
    attribuée à Edgar Allan Poe 
    (<a href="https://www.eapoe.org/works/mabbott/tom2t008.htm" 
      >lire l’original en anglais en ligne</a> ; 
    une traduction française est parue en 2018 chez Phébus 
    dans « Nouvelles intégrales, tome 1 »)
title: Un foutu cauchemar
...

# Un foutu cauchemar

L’autre jour, au crépuscule, j’ai fait escale dans un patelin, 
mes selles étaient vides et mon canasson avait chopé la crève. 
J’en menais pas large, je comptais juste me soûler vite fait, 
pioncer un coup et me tailler à la fraîche, mais visez un peu 
qui je croise, en pleine racontade devant un troupeau de 
niaiseux : le Nazaréen, ouais, comme je vous le dis. Et c’est 
pas tout, faut voir ce qu’il avait cousu au gilet, le Jessie : 
l’étoile du shérif. Lui, shérif ! Autant prendre un sauvage, 
l’accoutrer en bleusaille et le faire monter dans la cavalerie… 
Notez, la cavalerie, j’y avais bien traîné mes guêtres, moi, Ed 
le Coyote.

Cet enfant de putain se pavanait comme un coq, à conter ses 
histoires à qui mieux mieux autour du puits : comment à Little 
Rock il avait expédié en Enfer quarante Indiens féroces seul 
avec sa Winchester, guidé par le Créateur en personne, comment 
il avait sauvé toute la populace du bled, comment avant de se 
retirer il avait écumé la région avec ses gars et l’avait 
nettoyée de la vermine… Cause toujours. Tout à ses menteries, il 
m’avait pas vu débarquer. Y me tournait carrément le dos. C’est 
rien dire que la gâchette m’a titillé. Par les sept crotales, 
j’avais une balle avec son nom gravé dessus !

C’est à ce moment-là qu’y a eu un truc bizarre. J’ai tout vu ce 
qui s’est passé, c’est allé très vite, comment je l’ai descendu 
et le reste.

Ni une ni deux, je dégaine, je l’ajuste et je fais parler la 
poudre. Je fais mouche, mais j’ai pas visé là où qu’il aurait 
claqué fissa. Les badauds décanillent en couinant, comme ça le 
Jessie peut bien encadrer qui l’a plombé sans crier gare. Je 
m’écarte de vingt pas pour contempler feu le shérif de Windy 
Salem, la crapule la plus coriace à l’ouest du Mississipi. Il 
est pas encore refroidi, le Nazaréen : la vie s’accroche à sa 
charogne puante, visiblement pas pressée de marcher dans la 
vallée de la mort. Il sue comme un porc dans la poussière et ses 
yeux sont écarquillés, vous voyez, comme un poulet qu’on 
décapite. Alors je me plante là, je m’en grille une et j’attends 
qu’il crache son dernier souffle. Un râle bien pathétique que 
c’était, ouais, je vous le dis.

Oubliée ma carne malade, finalement la soirée commence sur les 
chapeaux de roues. Faut arroser ça. Je fais demi-tour et je me 
promène peinard dans la seule rue de Windy Salem. Je remarque 
les bourges qui se carapatent dans les baraques, je remarque 
leurs poches bien rondes, vous voyez ce que je veux dire. Je 
remarque leurs poules aussi, leurs miches bien rondes, vous 
voyez ce que je veux dire. Cette brave petite ville fait plaisir 
à voir. Oh ouais, les affaires vont être bonnes.

Au bout, y a l’ombre de la maison de Dieu qui s’allonge à vue 
d’œil et qui barre la rue. Pour l’instant, leur église, c’est 
quatre murs en pin blanc tout frais sur lesquels ils ont même 
pas encore posé de toit. Alors, braves gens de Windy Salem, qui 
va protéger vos petites fermes maintenant ? Z’êtes rien qu’un 
ramassis de pouilleux paumés dans le désert, rien ni personne 
viendra vous sauver. Votre shérif, je viens de l’expédier _ad 
patres_, et votre Seigneur, il a dû vous damner quand vous vous 
êtes mis en cheville avec cet enfant de salaud. Et tout ça pour 
que dalle, puisque ça m’a pas arrêté, moi, Ed le Coyote. Quand 
mes gars débouleront de la plaine, vous pourrez toujours prier, 
ça vous dispensera pas de payer.

Mais, pile quand le Soleil disparaît à l’Ouest, y a comme un 
vent froid qui se lève et qui charrie toute la poussière de la 
plaine. Une ville pareille, forcément, ça fait courant d’air. Je 
reboutonne le cache-poussière et je cache ma méchante gueule 
dans le col. Les bâtiments se fondent dans la purée. Le vent 
gémit, mais pas que : y a dans l’air des hululements de mauvais 
augure, à vous filer la chair de poule, même à mézigue. Faut 
dire, si c’est les sauvages qui rappliquent, je suis mal barré : 
me voilà-t-y pas en pleine rue comme le pif au milieu du 
trognon, mon cheval est hors course, je viens de vider mon colt, 
et de toute façon j’y vois pas à six pas.

Les youyous se rapprochent. J’avance au hasard, et je cloue 
carrément mon pif sur le portail tout neuf de l’église. Ça sent 
le sapin. Je me glisse dedans, et me faites pas dire ce que je 
dis pas, les bondieuseries m’impressionnent pas, mais là faut 
reconnaître que c’est spécial : dehors c’est le chaos, y a rien 
qui tient en place, mais dans l’église on entend que dalle, 
silence de tombe, et comme la maison de Dieu est à ciel ouvert 
y a la Lune qui éclaire, et de la poudre qui tombe tout 
doucement sur l’autel comme des petites pépites.

J’entends un sanglot étouffé. Dans un coin, une bonne femme 
serre son morveux qui tremblote comme une feuille. Rien 
à craindre de leur côté, alors je me retourne pour garder le 
portail à l’œil. Je reste pas mal de temps comme ça, silencieux 
avec les deux chouinards derrière… Mais tout à coup le calme est 
déchiré par un hurlement atroce. Je pivote en sursaut : là, 
derrière l’autel, une forme bouge ! Un de ces satanés démons 
à la peau rouge, il a les yeux fous, la langue qui pend, un 
grand couteau dans une main et dans l’autre des scalps 
dégoulinants de sang. Le saligaud est passé par la porte 
arrière, celle qui mène au futur cimetière de Windy Salem. Il va 
se remplir plus vite que prévu, le cimetière. Comme je tiens pas 
à l’inaugurer en personne, je tire ma révérence et je ressors en 
vitesse pendant que le sauvage taille en pièces la veuve et 
l’orphelin.

Dehors, c’est pire que l’Enfer. De tous les côtés des honnêtes 
gens braillent en se faisant saigner. Nulle part où se planquer.

Soudain, à l’est, on capte un clairon. Des sabots, des fusils, 
des ordres qui claquent. Le vent se tait, la poussière tombe, et 
voilà un régiment de cavalerie qu’est en train d’établir un 
périmètre autour du puits. Ils corrigent les Peaux-Rouges, ça 
oui, ces chiens galeux se tirent la queue entre les jambes… Mais 
la poisse me colle à la selle, faut croire, parce qu’ensuite ils 
m’avisent moi, ces soldats moustachus dans leurs jolis costumes 
bleus, et pour mon malheur je les replace, ils me replacent, on 
se reconnait tous : c’est les vauriens de ce maudit 11ᵉ. Ils ont 
ramassé le Nazaréen, l’ont bordé dans un beau drapeau, lui ont 
remis ses vieux galons de capitaine, et maintenant ils le 
hissent bien haut et lui font une haie d’honneur. Une haie 
d’honneur ! Ils me zyeutent avec des airs qui font froid dans le 
dos. Moi forcément, j’y vais pour détaler sans demander mon 
reste, mais ils m’ont comme qui dirait cerné. Ils m’agrippent, 
ils m’attachent les pognes, ils dressent un gibet et ils me font 
monter sur un cheval, ma vieille rossinante, les ordures. Je 
louche sur la corde, tout à coup je respire très fort, c’est pas 
possible, ils me passent la boucle autour du cou, le chanvre me 
pique l’échine. Je panique authentiquement quand y a le sergent 
qui pointe son fusil sur la caboche de mon bourrin. J’ouvre la 
gueule pour m’égosiller, il tire, ma monture s’effondre, le 
plancher des vaches m’aspire brusquement, la corde…

Le Jessie me tournait le dos. J’ai rengainé.

# vim: set tw=64 et ts=2 fo+=taw:
