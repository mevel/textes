---
date: 2022-04-18
summary: atelier d’écriture, en gros il fallait 
    (1) remplir une fiche de personnage 
    (2) la mettre en scène dans une salle d’attente 
    (3) la confronter à sa phobie 
    (exercice issu de <i>Noir sur blanc</i> par Mylène Fortin)
...

# « Camille »

<!--

FICHE PERSO:
- nom: Émilie / « Camille »
- âge: 25
- tic de langage: « allons bon », « quelle débile » (à propos 
  d’elle-même)
- tic/manie: se mordiller la lèvre inférieure
- phobie: agoraphobie, légère paranoïa
- valeurs: discrétion, indépendance
- caractéristiques sociales: célibataire, maîtresse d’école
- caractéristiques psychologiques: timidité, manque de confiance 
  en soi, angoisse
- caractéristiques physiques: 1m65, châtain, agile
- quête?

-->

Émilie — non, « Camille » —  serrait son sac contre elle, comme 
un bouclier contre les regards. Il y avait trop de monde. La 
salle était trop vaste et la rangée de chaises se trouvait au 
beau milieu, entre les portes et les guichets. Impossible de 
tout garder à l’œil en même temps. Le moindre bruit que 
quelqu’un entrait la poussait à se retourner. Elle aurait 
préféré se blottir dans un coin, quitte à rester debout, mais 
l’affichage annonçait trois quarts d’heure d’attente et cette 
attitude bizarre aurait attiré l’attention. L’impression la 
tenaillait que tous ces gens perçaient son masque.

Allons bon, ridicule. Ce n’était que de petits employés des 
services publics et des quidams venus retirer des papiers, comme 
elle. Aucun d’entre eux ne la connaissait. Dans une heure, tout 
serait réglé pour de bon.

Elle compara une énième fois le numéro sur son ticket à celui de 
l’écran, au-dessus des guichets. Elle surprit le regard d’un 
jeune homme derrière l’un des bureaux vitrés. Du calme ; il ne 
savait rien. Seulement, sa nervosité devait être trop flagrante. 
Ou bien il la trouvait jolie… Allons bon. Elle se mordilla la 
lèvre. Ou bien ce n’était rien du tout, un regard croisé par 
hasard.

Quand même… Elle voudrait bien ne plus être seule.

# vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—:
