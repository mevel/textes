---
date: 2014
summary: poème en tandem avec mon voisin de table
    (qui se reconnaîtra) pendant un cours de philo de spé ;-)
title: Quand j’étais petit
...

# Quand j’étais petit

. Quand j’étais petit, je n’étais pas bien grand.
. Que tu es petit, me disait ma maman.

. Un jour le déclic, je me mis à grandir.
. Quelle mouche te pique ? me faisait le fakir.

. À grandir je me mis, je vivais gaiement.
. Que tu as grandi, me disait Gaëtan.

. Petit à petit, je grimpais en hauteur.
. Ce que tu grandis ! me glissait le facteur.

. Ma croissance achevée, j’arrêtai de grandir.
. Pourquoi ne crois-tu plus, me demandait Mounir.

. Ma taille restant constante, s’accumulaient les jours.
. Que tu restes inchangé, me disait mon amour.

. Quand j’étais jeune homme, j’étais de bon aloi.
. Que tu es bonhomme, me disait Saint-Galois.

. Les années défilaient, on m’appela Papa.
. Est-tu un grand héros ? me demandait Nicolas.

. Je lui contais alors mes actes de bravoure.
. Ce qu’il est petit, me disait mon amour.

. À grandir il se mit, il vivait gaiment.
. Que tu as grandi, lui disait Gaëtan.

. Ce faisant moi aussi, j’allais en vieillisant
. Ta bedaine a grossi, me faisient les passants.

. Ses études terminées, Nicolas fiancé,
. Tu es un homme galant, finis-je par lui avouer.

. Puis de trop vieillir, petit je redevins.
. Pire est ta venir, me prévint le devin.

. Petit à petit, j’ai bien vécu mon âge.
. L’oiseau fait son nid, comme dirait l’adage.

## Remake de 2016 (brouillon)

. Quand j’étais petit, je n’étais pas bien grand
. Que tu es petit, me disait ma maman

. Cette courte période n’a duré qu’un moment
. Car avec méthode, je suis devenu grand

. Tout a commencé par mes premiers efforts
. Pour assez pousser, j’ai dû m’y prendre tôt

. Au petit matin, je m’étirais le dos
. Ce qu’il est malin, me mentait mon mentor

. À midi sonnant, je poussais du menton
. Je serai géant, je disais à tonton

. Tout l’après-midi, je croassais aussi
. Ce que tu grandis, félicitait Lucie

. Le soir à la soupe, je grandissais encore
. Je buvais ma coupe, il fallait cet effort

. La nuit dans mon lit, je m’allongeais sans trêve
. Je n’avais de répit qu’en sombrant dans les rêves

. Petit à petit, j’ai poussé ma croissance
. Ce que tu grandis, me répétait Maxence

…

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
