---
date: 2021-09-13
modified: 2024-05-22
summary: atelier d’écriture, thème « Une rencontre »
  (voir aussi
  <a href="https://www.bedetheque.com/serie-567-BD-Caste-des-Meta-Barons.html"><i>La Caste des Méta-Barons</i></a>,
  <a href="https://xkcd.com/2779/">xkcd#2779</a>,
  <a href="https://escaleajeux.fr/jeu/gareq.0.1">Gare au Requin !</a>)
...

# La rencontre

Dans les cieux infinis, la violence avait atteint son paroxysme. 
Depuis des hypercycles dont les batailles avaient perdu le 
compte, les deux clans frères s’étaient abimés dans une Guerre 
de Sang. La lutte était totale. Elle n’aurait d’autre issue que 
l’annihilation de l’un des belligérants — ou des deux.

Les hommes ne connaissaient plus que le crépitement des 
plasmo-canons, l’odeur du fer chauffé et de la chair grillée. La 
jeunesse innocente, l’avenir des deux clans, se propulsait 
à corps perdus à travers les astronefs de l’autre dans des 
modules kamikazes. Les grands vaisseaux d’acier, fiertés des 
capitaines, ne prenaient plus le large que pour alimenter cette 
fureur hurlante : se bombarder, se percuter, se fracasser et, 
pour finir, débris fantômatiques enchevêtrés ensemble, dériver 
à jamais délivrés de la haine. Seules pleuraient encore les 
étoiles lointaines. La soif de sang, la soif de fer jamais ne se 
tarit.

Mais il fallait conclure. Lorsque le grand Therak, fier 
archi-commandant du clan du Fer d’Orion, massa ses amiraux, 
capitaines et vaisseaux dans le Nuage d’Oort, chacun sut. Le 
temps était venu de l’ultime bataille. Galmor le Rougeoyant 
rassembla tout le ban du clan Cosa Nova, l’arma et le porta au 
devant du combat. D’un côté comme de l’autre, les voiles 
voltaïques étaient gonflées du vent de la Destinée.

Les engins se firent face en formant une sphère, tous leurs 
canons dehors. Deux murs de mort.

Dans leurs combinaisons de palladium azur, les deux princes 
sortirent. Orgueilleux, impériaux, entourés de leurs sbires,
ils s’avancèrent seuls dans le champ de bataille. Ils 
s’immobilisèrent à quelques encâblures, photo-capes claquant 
dans le vide spatial. Le silence était pur, et l’attention 
totale.

Les frères ennemis s’affrontaient du regard. Un protocole 
ancien, implacable et complexe gouvernait la rencontre. Les deux 
archi-commandants le connaissaient sur le bout des doigts. Il ne 
fallait pas en dévier d’un iota.

Ils crispèrent imperceptiblement leur poing cybernétique. 
Balindur l’Ancien murmura :

— Ça va commencer.

Les hommes retinrent leur souffle. D’une voix de commandement 
qui retentit dans tous les transmetteurs, Therak l’ainé déclama 
l’amorce rituelle :

— Check en haut !

Galmor s’époumona :

— Check en bas !

Therak reprit :

— Tous les doigts !
— _Ta-ta-ta_ !
— Pistoblast !
— _Piou-piou-piou_ !

Les peuples des deux clans assistaient aux salutations codifiées 
de leurs archi-commandants, fascinés par leur danse aux 
métaphores mortelles aussi bien que sensuelles.

— Gaga, maudit grigou !
— Vieille canaille de frérot !
— Commandant de plaisancier !
— Amiral de recycleur !
— Ferrailleur !
— Naufrageur !
— Cancrelat de l’espace !
— Charlot volant !
— Tu sais ce que je veux, forban.
— Cause toujours, malandrin.
— Je veux ma victoire, Gaga.
— Ta victoire ? C’est moi qui ai gagné, frérot. Tous les pirates 
  qui croisent dans les cieux infinis ont été témoins de mon 
  triomphe écrasant.
— Ils t’ont vu tricher comme tu triches toujours. Maman était 
  trop gentille avec toi. Mon poisson était devant tout du long.
— C’est le jeu, frérot. Mauvais jet, pas de chance.
— Dans ce cas, je ne vois qu’une seule issue, Gaga.
— Sérieusement, tu veux remettre ça ?

Drapé dans sa majesté, conscient de faire Histoire, l’ainé des 
deux archi-commandants édicta en détachant tous les mots afin 
que chaque amiral, chaque capitaine et chaque matelot entendît 
ses paroles :

— Galmor le Rougeoyant du clan Cosa Nova ! Moi, Therak le Très 
  Grand du clan du Fer d’Orion, je te défie, toi et tes gens, au 
  nom des miens ! Puisse votre poisson nourrir le gros requin !

L’assemblée était suspendue à la réponse de son ennemi, son 
frère. Celui-ci redressa sa stature :

— Le clan Cosa Nova entend le Fer d’Orion ! Il répondra 
  présent ! Son poisson n’aura rien, car c’est celui du Fer qui 
  nourrira Zinzin !

Alors vint le tonnerre : celui des hurlements, déchainés, 
hystériques. Une excitation primordiale et sans borne s’était 
emparée des pirates, qui exultaient d’incompréhensibles 
borborygmes jusqu’à couvrir leurs visières de bave. Le 
vaisseau-amiral de Cosa Nova ainsi que celui du Fer d’Orion 
s’ouvrirent avec d’affreux grincements. Les deux gueules de fer 
libérèrent deux modules autonomes : deux poissons mécaniques, 
qui vinrent s’aligner dans un rayon du champ de bataille, deux 
satellites autour des rois-soleils.

Avec délicatesse ces derniers détachèrent leur Dé de 
commandement d’une poche de leur scaphandre d’apparat.

Therak se questionnait : avait-il fait le bon choix ? 
N’allait-il pas subir un nouveau revers ? Les dés du Destin 
n’étaient-ils pas truqués ? Non, il devait cesser de douter. 
Cesser de penser. Se concentrer sur son Dé. Sentir, entre son 
pouce et son index, ce petit cube d’or forgé par ses ancêtres et 
poli par les siècles. Se tenir prêt à le lancer ; lire son 
numéro ; recommencer. En cet instant fatal, cela seul 
compterait.

Les cycles s’égrenaient. Les lents cercles de plomb tracés par 
les poissons s’estompaient peu à peu dans l’impassible éther.

Une pulsation, plus grave qu’un quasar, fit vibrer le Nuage. 
Puis une seconde onde. Une troisième. C’était l’appel. Alors une 
secousse ébranla le tissu de l’espace-temps. La folie des hommes 
était devenue indescriptible. Tous scandaient dans leurs 
casques, et les radios saturaient de cette clameur rythmique :

— Zin-zin ! Zin-zin ! Zin-zin ! Zin-zin !

Surgissant d’une faille oméga-scénaristique du troisième type, 
une monstrueuse mâchoire de palladium se matérialisa dans la 
sphère. Zinzin, l’abominable requin spatial, était venu. Malheur 
au poisson qu’il attraperait en premier ! Gare à Zinzin !

De concert, les deux archi-commandants lancèrent leur Dé.

# vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—:
