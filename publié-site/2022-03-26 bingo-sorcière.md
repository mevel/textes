---
date: 2022-03-26
modified: 2024-02-06
summary: atelier d’écriture (InterQ 2022), caser les mots
    « <span class="spoiler">pelote, sorcière, frémir, main, 
    violette, astronomique</span> »
    (voir aussi <i>Le Feu de la Sor’cière</i>,
    2e pire roman que j’aie jamais lu)
...

# La nuit de la sorcière

Par pur plaisir, Jenny inspecta le ciel une dernière fois avec 
sa violette astronomique. Ce qu’elle y vit la fit frémir 
d’excitation. Anouk ne la croirait jamais ; bien fait pour elle, 
elle n’avait qu’à venir. Il n’y avait aucun doute : sur fond de 
Voie lactée, Vénus — l’étoile du berger — était comme toujours 
reconnaissable entre mille ; le disque rouge de Mars, qui 
n’avait pas cessé de gagner en magnitude au cours de la semaine, 
était en cette nuit de juin plus net que jamais ; quant à ce 
petit point beige qui venait d’apparaitre à l’œil nu entre les 
deux autres planètes, et dont la violette révélait les anneaux 
majestueux, ça ne pouvait être que Saturne.

De l’autre côté de la voute céleste, bien sûr, la Lune était 
pleine. La nuit rêvée pour une sorcière.

Jenny claqua des mains. « Bingo ! » pensa-t-elle. Elle tournoya 
vivement sur elle-même, s’emmêla les pieds dans une pelote de 
laine abandonnée dans l’herbe humide et manqua s’étaler de tout 
son long en pouffant de rire. Elle l’avait oubliée, celle-là ! 
Tout autour de la jeune fille s’étendait sa toile des étoiles, 
une carte des constellations qu’elle avait minutieusement tissée 
avec des bâtons, des aiguilles et du fil à tricoter blanc 
— argenté sous la Lune — sur lequel elle laissait rêveusement 
courir ses doigts, d’habitude…

Elle se reprit l’instant d’après : assez d’enfantillages. 
Contrairement à Anouk, elle n’était plus une petite fille. Tout 
compte fait, elle était contente que son amie ait refusé de 
l’accompagner ce soir en prétextant un stupide malaise. Jenny 
n’avait pas de temps à perdre avec ses caprices.

Tout de même… Elle y était ! L’alignement de Blake, celui qui 
d’après le vieux bouquin poussiéreux ne revenait que tous les 
96 ans ! On disait qu’alors les astres dotaient une nouvelle 
femme de pouvoirs magiques.

Les légendes des sorcières avaient toujours fasciné Jenny. Quand 
elles étaient petites, elle et Anouk allaient souvent jouer 
à la Dame aux Corbeaux parmi les arbres ou dans le vieux grenier 
tout craquant d’Anouk qui, avec ses poutrelles basses et ses 
nombreuses trappes, leur évoquait une volière. La maison au toit 
vert de son amie se trouvait en lisière d’un bois ancien ; quand 
un grimoire, découvert dans une malle du grenier, leur avait 
révélé que c’était justement dans ce bois qu’était née 
l’illustre femme, l’imagination de Jenny s’était enflammée. 
Évidemment tout le monde connaissait la Dame aux Corbeaux mais 
elle, Jenny, vivait dans le pays même où la sorcière avait vécu, 
*elle* marchait sur ses traces.

Selon le livre, la forêt était autrefois bien plus vaste ; elle 
couvrait alors ce qui était à présent les verts pâturages que 
Jenny connaissait par cœur. Quand la Dame arpentait les bois et 
parlait aux oiseaux noirs, c’était comme quand elle-même courait 
dans les herbes à la poursuite d’un renard. Si ça se trouvait, 
la sorcière était son arrière-arrière-grand-mère ! En apprenant 
que le prochain alignement devait avoir lieu dans deux ans, peu 
avant son anniversaire, elle en avait acquis la conviction : 
elle serait la prochaine sorcière.

Anouk ne l’avait pas prise au sérieux. Jenny soupira. Son amie 
avait été beaucoup moins drôle, tout à coup. Jenny avait passé 
de moins en moins de temps avec elle tandis qu’elle se préparait 
à devenir la plus grande sorcière de tous les temps.

Tout d’abord, elle avait étudié l’astronomie grâce au grimoire. 
Elle s’était procuré une violette d’amateur — cela suffisait 
pour son usage —, avait appris à reconnaitre les objets 
célestes, les constellations et ainsi de suite. Elle avait eu 
l’idée d’assembler cette grande toile qui lui avait rendu un 
fier service pendant ses nombreuses nuits d’observation. Elle en 
caressa le fil avec reconnaissance. Elle n’en aurait plus besoin 
quand elle serait sorcière.

Elle avait également lu toutes les histoires sur les sorcières. 
Elle s’était souvent demandé ce qu’elle ferait de ses propres 
pouvoirs. Pourrait-elle lancer des sorts, voler sur les vents, 
écouter leur murmure ? Aurait-elle un animal fétiche comme la 
Dame aux Corbeaux ? Peut-être pourrait-elle se faire appeler la 
Dame aux Renards ? Ça lui irait bien, sa crinière était aussi 
rousse que celle de son héroïne était noire — noir de jais, 
comme une corneille, c’était dans tous les contes. De quels 
terribles dangers devrait-elle protéger la Nature et les 
Hommes ? Et puis… Oh, elle verrait bien quand elle serait 
sorcière.

Et puis il y avait une dernière chose… Le ciel désignait une 
femme, une vraie femme, qui avait accédé à l’âge adulte. « _Le 
feu de la sorcière reconnaitra son sang._ » À la lecture de ce 
passage, Anouk avait froncé les sourcils assez fort pour les 
joindre en un trait brun comique, mais Jenny n’était pas une 
ignorante et avait tout de suite compris. C’était très clair, en 
fait. Elle savait que cela viendrait et qu’elle n’avait rien 
à faire, mais elle avait guetté l’événement avec une inquiétude 
croissante à mesure que l’échéance céleste approchait. 
Finalement, il y avait à peine un mois, c’est presque en 
exultant qu’elle avait accueilli la crampe ; elle était devenue 
une femme faite. Elle s’était efforcée de ne pas trop montrer 
son soulagement devant Anouk. Après tout, ça n’était qu’une 
confirmation supplémentaire, s’il en fallait, de son destin de 
sorcière.

Jenny jeta un regard au loin. Elle s’était installée au sommet 
de la plus haute colline, d’où son regard embrassait toute la 
prairie et jusqu’à la masse plus sombre du Bois des Corbeaux, 
tout là-bas, sous l’alignement planétaire. Elle devinait même 
l’endroit où, de jour, se distinguait la maison au toit vert 
d’Anouk. Peut-être, malgré tout, la gamine l’épiait-elle en 
secret ?

Elle plaignit sa jeune amie : même une fois adulte, celle-ci 
resterait toute sa vie une paysanne et devrait se contenter d’un 
garçon de ferme ignorant ou d’un villageois balourd. Elle voyait 
les spécimens du pays, non merci ! Tandis qu’elle-même 
deviendrait une personnalité très importante et, de plus, 
c’était bien connu, les sorcières possédaient toujours une très 
grande beauté — son aïeule plus que toutes. Jenny voyagerait 
beaucoup et aurait à ses pieds des tas d’hommes intelligents, 
beaux et raffinés. Elle n’aurait qu’à choisir. Elle poussa un 
nouveau soupir, plus dramatique cette fois — elle s’entrainait. 
C’était injuste, mais qu’y pouvait-elle ? Il ne pouvait y avoir 
qu’une seule sorcière.

Tout à coup le ciel s’illumina. Une lueur rosée s’amassa dans le 
prolongement des trois planètes alignées. Jenny écarquilla les 
yeux, émerveillée. Elle trépigna tandis que le phénomène gagnait 
en intensité. Toute la prairie, les frondaisons du Bois des 
Corbeaux, ses robes, ses mains tendues… Dans l’obscurité 
nocturne, formes et contours étaient soulignés de chaleureux et 
fantasmagoriques dégradés de rose. Enfin, un arc lumineux 
jaillit, qui n’avait rien d’un éclair d’orage, et descendit 
lentement, gracieusement, sur la maison au toit vert d’Anouk.

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
