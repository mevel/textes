---
date: 2011
summary: euh… bon ?
title: L’Assemblée
...

# L’Assemblée

_Une session ordinaire à l’Assemblée…_

**le Secrétaire Général** — Messieurs, s’il vous plaît ! 
Première affaire à l’ordre du jour présent d’aujourd’hui : nos 
enquêteurs de terrain rapportent une colère sourde.

**un Acadame d’âge moyen pour un Acadame** — Que dit-il ?

**son voisin** — Une colère sourde !

**le Secrétaire Général** — Il semblerait que les 
puéricultrices… teurs… souffrent qu’on les confondent encore 
avec les capillicultrices… teurs… On voit de tout… D’après 
elles… eux… euh, d’après la profession, leur chiffre d’affaire 
en pâtirait. Elles… ils nous font parvenir des… nombres…

**le duc de Fleûry-de-Haûtcâstel** — Quelle solution, Monsieur 
le Secrétaire Général, nous proposent les membres de cet 
honorable corps de métier ?

**le Secrétaire Général** — Aucune, ô duc de 
Fleûry-de-Haûtcâstel, qui soit parvenue à nos agents.

_[indignation générale]_

**un Acadame** — Allons bon ! Pour qui se prennent ces… 
puéricultrices ?

**un autre** — Et pour qui *nous* prennent-elles ? Ils ne nous 
appartient pas de trouver des solutions. En tout cas, ça ne 
figure pas dans nos indemnités !

**un autre** — Les Éternels, des solutionneurs !

**un autre** — Incroyable !

**un autre** — Ont-elles seulement lu les Statuts ?

**un autre** — Les humbles greffiers de l’usage, faut-il le 
répéter…

**le Secrétaire Général** — À vrai dire, nos services ont reçu 
une… Ces personnes ont obtenu l’appui… S’il vous plaît ?

**le duc de Fleûry-de-Haûtcâstel** — Du calme, allons, écoutons 
Monsieur le Secrétaire Général.

_[tous se calment]_

**le Secrétaire Général** — Merci, duc. Ces puéricultrices… 
teurs… disposent du soutien de la Concédération.

_[panique]_

**plusieurs** — Fiel ! La Concédération !

_[le Président, jusque là amorphe, remue légèrement]_

**le Président**, _très lentement_ — Et quels sont les termes de 
la Concédération ?

**le Secrétaire Général** — « Les membres représentants de la 
Concédération à l’Assemblée des Acadames. Nous avons pris acte 
du mécontentement du corps puéricole… » _puéricole_ !

_[sifflements de dents]_

**le Secrétaire Général** — « puéricole, et vous faisons savoir 
que nous soutenons leur demande. »

_[re-panique]_

**plusieurs** — Ciel !

**un Acadame** — C’est grave.

**un autre** — Nous sommes perdus !

**un autre**, _à part_ — Vous souvenez-vous de l’ancien 
Secrétaire Général ?

**son voisin**,  _à part_ — Pauvre Secrétaire Général !

**le duc de Fleûry-de-Haûtcâstel** — Un peu de rigueur, voyons ! 
Raisonnons calmement, comme à l’ordinaire.

**le Secrétaire Général** — Alors, messieurs, des suggestions ?

_[tous se concertent et murmurent ; après un temps :]_

**le Premier Sous-Secrétaire** — Nous pourrions envisager de 
constituer un groupe de réflexion

**le Second Sous-Secrétaire** — dont le travail préliminaire,

**le Premier Sous-Secrétaire** — échelonné sur cinq à six mois,

**le Second Sous-Secrétaire** — dégagerait la voie pour un débat 
circonstancié, ici-même,

**le Premier Sous-Secrétaire** — d’où sortirait

**le Second Sous-Secrétaire** — si nécessaire !

**le Premier Sous-Secrétaire** — la première ébauche d’un 
communiqué officiel

**le Second Sous-Secrétaire** — ainsi que la constitution d’une 
commission spécialement déléguée

**le Premier Sous-Secrétaire** — chargée de mûrir le projet sous 
les orientations impulsées par le groupe de réflexion 
préparatrice et l’assemblée pleinière

**le Second Sous-Secrétaire** — par des réunions régulières

**le Premier Sous-Secrétaire** — disons semestrielles,

**le Second Sous-Secrétaire** — avec en ligne de mire la 
publication définitive,

**le Premier Sous-Secrétaire** — éventuelle, et en bonne entente 
avec le pendant commissionnaire de la Concédération,

**le Second Sous-Secrétaire** — dudit communiqué au Journal 
Officiel

**le Premier Sous-Secrétaire** — d’ici trente à quarante mois,

**le Second Sous-Secrétaire** — soit pour la rentrée scolaire.

**le Premier Sous-Secrétaire** — Une rentrée scolaire, à tout le 
moins !

**le Secrétaire Général**, _visiblement soulagé_ — Excellent, 
voilà qui me paraît tout à fait raisonnable. En fait, c’est même 
assez brillant. Qu’en pense l’Assemblée ?

**tous** — Faisons cela !

**le Secrétaire Général** — Nous retenons cette préposition.

**tous** — Hourra pour les Sous-Secrétaires !

**le Secrétaire Général** — Affaire suivante. Ces mêmes 
puéricultrices… puériculteurs, les puéricultrices et 
puériculteurs, passez-moi le pléonasme, se plaignent que le mot 
_puéricole_ n’existe pas. Outre que cela gênerait l’exercice de 
leur profession, ils se prétendent lésés, je cite, « dans notre 
État de droit »…

**certains** — « Notre État de droit ! »

**le Secrétaire Général** — de ne pas être traités à égalité 
avec les « agriculteurs et agricultrices, viticulteurs et 
viticultrices, apiculteurs et apicultrices, arboriculteurs et 
arboricultrices, protoculteurs et protocultrices, 
capilliculteurs et capillicultrices… »

**certains** — Encore eux !

**le Secrétaire Général** — … et ils joignent une liste plus 
complète mais je crois que cela suffira à saisir l’idée.

_[nouveau tollé ; plusieurs quittent l’hémicycle]_

**le Préposé Permanent de Principe à la Lettre P**, _qui a bondi 
sur ses pieds_ — Palsambleu ! En à peine trois siècles, j’ai 
déjà dû encaisser _poster_, _porridge_ et _participatif_. Je ne 
souffrirai pas de consigner sous la contrainte un barbarisme 
immonde de plus. La peste de ces puéricultrices, qu’elles 
fassent une périphrase ! En vérité, je vous l’affirme, si cette 
préposition indécente devait être actée, je prendrais les 
devants : il vous faudrait trouver un autre Préposé.

**le Coordinateur Pangrammatique de l’Alphabet** — Monsieur le 
Président, ce que vous entendez là, ce n’est pas seulement l’ire 
légitime d’un Préposé Permanent de Principe à la Lettre P ; 
c’est aussi celle d’un Commissaire Critique de la Lettre C, d’un 
Archiviste Assigné à la Lettre A, et je vous passe les autres. 
Je vous le disais déjà le mois dernier, tandis que je luttais 
farouchement aux côtés du Zoologiste Zététicien de la Lettre Z 
_[celui-ci opine]_ contre l’adoption hors de propos de… hem ! 
_zyeuter_. Les lettres sont taboues. Le Dictionnaire n’en peut 
plus de cette vague de légalisations sauvages qui déferle sur 
lui. Les mots anciens et honorables ne se sentent plus en 
sécurité. Ainsi donc, _zygène_, un honnête mot de souche 
gréco-latine, a vécu jusqu’au dernier moment dans la crainte 
d’être affligé d’un voisin aux origines troubles et aux manières 
suspectes, pour ne pas dire plus. Nous ne pouvons nous permettre 
d’adopter tous les mots du caniveau. Songez-y : hier 
_participatif_, aujourd’hui _puéricole_, demain quoi ? 
_Pataquessiaque_ ? Ah ! je l’ose à peine imaginer. Non, 
Monsieur, Concédération ou pas Concédération, j’en appelle 
à votre discernement.

**le Premier Sous-Secrétaire** — Les Acadames ne sont plus ducs, 
Monsieur le Président !

**le duc de Fleûry-de-Haûtcâstel** — Je le suis, moi.

**le Second Sous-Secrétaire** — C’est juste. Les Acadames ne 
sont plus dupes, Monsieur le Président !

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
