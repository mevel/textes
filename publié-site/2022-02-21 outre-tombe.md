---
date: 2022-02-21
summary: atelier d’écriture, thème « Voix d’outre-tombe »
    (voir aussi <i>La trilogie de Bartiméus</i>)
...

# Le prisonnier

.
. Entre tous les esprits qui peuplent l’Autre Monde,
. Aussi forts que le Fleuve, aussi libres que l’air,
. Ivres du Soleil chaud sur le vaste désert,
. C’est lui qu’avait choisi la reine moribonde.

.
. On avait convoqué son âme vagabonde.
. Par le kyphi sacré et les rites mortuaires,
. On l’avait enchaîné sous la voûte de pierre
. Pour veiller à jamais la dépouille inféconde.

.
. Son univers est sombre, étroit et solitaire.
. Les canopes sont froids et tout tombe en poussière.
. Absurdité du Temps.

.
. De siècle en millénaire, une douleur profonde
. S’immisce avec le sable ; et sa colère gronde.
. Il attend. Il attend.

<!--------------------------------------------------------------

ALTERNATIVE POUR lA 3E STROPHE:
.
. Sa maîtresse a vogué {vers l’Occident d’éther / dans la barque solaire}.
. Le sarcophage est froid et tout tombe en poussière.
. Absurdité du Temps.


LEXIQUE:

un hypogée (tombe creusée sous le sol)
caveau
un/une canope, un vase canope
le kyphi (sorte d’encens sacré = poudre odorante à brûler)
encens
myrrhe


RIMES:

+ monde
  immonde
  inonde
  abonde
  nauséabonde
+ moribonde
  furibonde
+ vagabonde
  onde
  sonde
  seconde
. profonde
+ féconde
  fonde
. gronde

  tombe
  outre-tombe
  catacombe
  hécatombe
  (re)tombe
  bombe
  combe
  incombe
  succombe
  plombe
  surplombe

  éther
+ air
  terre
+ pierre
  calcaire
  lumière
  lunaire
  solaire
  clair
  atmosphère
+ poussière
+ désert
  rivière
  mer
  colère
  guerre
  austère
  caractère
  matière
  univers
  frontière
  vert
  primaire
  première
  dernière
  hivers
  chair
  enfer
  cimetière
  funéraire
+ mortuaire
  barrière
  étrangère
  découvert
  ère
  ordinaire
+ solitaire
  mystère
  entière
  légère
  éclair
  Le Caire
  fier
  clerc
  serre

-->

# vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:.:
