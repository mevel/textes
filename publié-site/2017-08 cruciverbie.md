---
date: 2017-08
modified: 2020-02
summary: 
    ayant la flemme d’expliquer, je laisse le lecteur deviner
    la contrainte supplémentaire qui a guidé l’écriture de ces
    <a href="https://plint.a3nm.net/fr/">alexandrins classiques</a>
title: La Cruciverbie
...

# La Cruciverbie

.
. je m’en vais vous narrer mon étonnant voyage
. en un lieu bien réel – ce n’est pas un mirage !
. maints marchands égarés, las, hâlés par les lis
. l’ont vu sur l’horizon, encadré par deux plis
. mains sur l’obi serré, lés sur les bâts des ures
. marchant, plus de pain bis, plus d’ers, plus d’eau, brûlures
. errant dans l’erg aride où Ra darde ses rais
. rets d’aas fulminants mais, si loin… ces cyprès ?

.
. contrant ces rus de rocs qui cerclent son orée
. la forêt d’ifs entés, fière nef, est gréée
. de l’océan de sable ou de ces arbres hauts
. on ne sait plus qui semble élancer les assauts
. dans l’ombre des brisants où se ruent les rus rouges
. s’ancrent cent pieds haleurs, fûts en tés et bois bouges
. la forêt s’arc‐boutant tend ses prises au vent
. pour qu’un fertile essaim la projette en avant
. et d’éons en éons, ce cancer vert bizarre
. sur l’érébien désert conquiert are après are
. bataille formidable et subtile d’aïs !
. juteux vers pour longs lais ou courts haïkaïs !

.
. marchands tus, becs béés : comment peut‐il survivre
. ce bois de l’au‐delà tout droit sorti d’un livre ?
. il boit l’eau de l’Aa, le véritable Aa
. qui dispute aux aas un bouillant brouhaha
. l’eau rugit, l’erg mugit, dur échange de thermes
. l’eau n’est plus que vapeur et lave les sols fermes :
. où Rê confronte Gê, rais lasers, jets azurs,
. sont d’arables rias pour les germes futurs

.
. ces confins de mystère au parfum d’Arabie
. ont un nom : l’oasis de la Cruciverbie

<!--------------------------------------------------------------

R:
    +Ra = Rê
    Rais = Retz (pays de Rezé, Gille de…)
    Ré (île de Charente)
    ras +rai +rets (filet) +ru ri +ria
    réer (bramer) +errer ère +ers (herbe pour nourrir le bétail)
    ara +are ire
    +ure = urus [yrys] (bœuf sauvage)
    Ur (ville antique)
    +ruer rouer(ie)
    +erg (désert de dunes) / reg (de pierres)
    mire(r)

T/D:
    touer (haler, tirer un bateau)
    +tu (taire) ut ute
    tan
    +té +enter têt (é)têter
    tee
    dé
    ter der
    terrer
    tuf +fût
    tek
    ide (poisson rouge)
    ode
    êta
    Etna

L:
    ale (bière)
    haler (tirer un bateau à la corde)
    +hâler (bronzer)
    el ("le" espagnol)
    oïl
    +las
    +li (unité de longueur chinoise)
    +lé (longueur de tissu)
    lier
    +lai (ballade épique celte)
    laie (sanglier femelle)
    léser

AUTRE:
    +Aa (nom de plusieurs rivières du Bénélux)
    +aa (coulée de lave, mot hawaïen)
    +aï
    +éon Éon
    huer
    Ha Ag
    +gréer gué égo
    +Gê = Gaïa
    onc
    +if +nef ufe
    ès esse us os saï sen yen
    oser
    +bât +bis ibis +obi +bée
    na né no nu
    ASA SA SR OPA OPE ope CEI ULM
    Pô pal
    cep
    émeu

jeu de mots à 2 la noix : "vent de force 3"

-->

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
