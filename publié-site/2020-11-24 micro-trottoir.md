---
date: 2020-11-24
modified: 2024-03-18
summary: atelier d’écriture, thème « Au hasard dans la foule »
...

# Au hasard dans la foule

<p style="font-size:125%;">
<strong style="font-variant:small-caps;">Tout a commencé</strong>
par un banal reportage d’opinion. En quelques semaines, elle est 
devenue malgré elle la star incontournable des micro-trottoirs. 
Aujourd’hui, c’et la femme la plus recherchée de France. Comment 
en est-on arrivé là ? Enquête.
</p>

7H40 sur le parking de France 2 : devant la fourgonnette aux 
couleurs de la chaîne, Tifaine attend son équipe avec une 
cigarette. Mathis, journaliste comme elle, et Jérôme le 
perchiste la rejoignent. Il ne manque plus que le caméraman. 
« _Bernard est toujours en retard (rires)._ » Le mardi matin, 
pour le quatuor, c’est micro-trottoir dans les rues de la 
capitale. « _On aborde tous les sujets. Le but est de rendre un 
portrait fidèle de la vie quotidienne des Français, de leurs 
états d’âme, […] loin des vues d’ensemble trop abstraites._ » 
« _On est au plus proche des préoccupations de nos 
concitoyens._ »

En quelques années, le micro-trottoir s’est imposé dans la 
presse généraliste — qu’elle soit imprimée, télévisée ou 
radiophonique — comme un format atypique et populaire. Selon la 
formule de Jean-Jacques Michel, PDG de France Télévisions, 
« _les Français parlent aux Français._ »

Pour un micro-trottoir réussi, un bon échantillonnage est un 
ingrédient indispensable. Les personnes interrogées doivent être 
statistiquement représentatives de la population générale. Pour 
cela, les journaux s’appuient sur ce que les scientifiques 
appellent la Loi des Grands Nombres : en abordant au hasard 
suffisamment d’individus dans les rues de Paris, on obtient un 
panel représentatif des Français de tous horizons. Tifaine le 
résume ainsi : « _Un bon micro-trotting, c’est 30 % de repérage, 
30 % d’approche et 50 % de chance. Sans oublier le montage._ »

8H20. L’équipe se trouve sur la place Saint-Michel, cachée 
derrière une fontaine, pour ne pas fausser l’expérience en 
perturbant les passants. Le sujet d’aujourd’hui : les fruits et 
légumes locaux. « _On veut cerner les habitudes de consommation 
des Français, notamment en ce qui concerne les produits dits 
bios, leur perception de ces produits._ » En réglant leurs 
instruments, ils cherchent du regard des personnes susceptibles 
d’être micro-trottées. « _Là, la vieille avec son petit 
chien !_ » Tifaine bondit, suivie par son équipe. « _Bonjour 
Madame, est-ce que vous faites vos courses…_ » « _Oh non non…_ » 
La dame repousse ses avances. Les refus ne sont pas rares. 
« _C’est tout à fait normal, ça fait partie du métier,_ » 
affirme Mathis tandis que Tifaine se lance à la poursuite de la 
retraitée, qui a pressé le pas. « _S’il vous plait… C’est pour 
France 2… Vous revenez du marché ? C’est des poireaux que je 
vois dans votre sac ? Ils sont bios ? C’est la saison des 
poireaux, n’est-ce pas ? Madame ? Madame…_ » La vieille franchit 
le pont et Tifaine revient vers ses collègues en serrant les 
dents. « _Ah, la garce ! Elle va me le payer au montage, on va 
pas la rater mon Bébère._ »

Bernard la console en lui désignant une proie facile : un bobo 
d’une trentaine d’années sortant d’une librairie. Tifaine 
rajuste son col. « _Bonjour Monsieur, faites-vous vos courses au 
marché ? Que pensez-vous de consommer des produits locaux ? 
Faites-vous attention à la saison des légumes ? Avez-vous un 
potager ? Quel est votre légume préféré ? Crème fraîche ou pas 
dans le risotto poireaux-champignons-parmesan ?_ » Volontaire, 
l’interrogé répond à toutes les questions pendant que Mathis 
prend des notes. Toutes les questions sont scriptées : « _Pas 
question d’improviser, il faut que tout le monde réponde au même 
interrogatoire. C’est essentiel pour la méthode statistique._ »

Après avoir relâché le jeune homme — non sans avoir décroché son 
06 pour les besoins de l’enquête —, Tifaine confie : « _S’ils 
pouvaient tous être comme lui…_ » Regard entendu entre nos 
journalistes. Ils pensent à Elle. Elle, la passante idéale que 
tous les micro-trotteurs rêvent de croiser depuis trois mois. 
Elle a fait sa première apparition à 20H12 le 8 août sur France 
Info, où Simon Baudrieux l’a micro-trottée sur la taxe 
carburant. Une femme au hasard dans la foule. Quatorze secondes 
magiques. Dans le milieu, l’intervention a tout changé. Mathis 
se souvient : « _Quand je l’ai entendue, j’étais dans ma 
voiture. Pour n’importe quel journaliste, c’était flagrant : 
Elle était absolument moyenne. C’était fantastique._ » Très 
vite, les appels affluent chez France Info. Tous les reporters, 
de toutes les rédactions, veulent savoir où Baudrieux a débusqué 
l’oiseau rare. Évidemment, la protection des sources prime.

Alors, dès le lendemain, les descentes dans les rues se 
multiplient. Tout sujet devient prétexte au micro-trotting. D’un 
reportage par quinzaine, on passe à deux par jour. Du Monde au 
Figaro et de TF1 à Gulli, tous se jettent dans la course à 
l’oiseau rare. Les experts analysent les quatorze secondes de 
l’extrait sonore, mais les indices sont maigres : « _une femme 
entre deux âges, quelque part à Paris, près d’un bus […]._ » 
BFM TV installe un coûteux dispositif de spatialisation des sons 
pour reconstituer la configuration des lieux. La Chaîne 
parlementaire se penche sur les données INSEE de la sociologie 
francilienne. Comme Tifaine, de nombreux journalistes sont 
reconvertis dans le battage médiatique du pavé. « _Ça a été 
déstabilisant au début : avant, je faisais les conférences de 
presse. Là, c’est très différent._ » Tifaine se prend vite au 
jeu. « _C’est stimulant et très enrichissant. On rencontre 
tellement de gens différents. Oh le gros là, il nous le faut._ »

Comment les Parisiens vivent-ils cette intensification du 
micro-trotting ? Nous sommes allés à leur rencontre. Comme 
Hervé, 54 ans, électricien, beaucoup n’en peuvent plus : « _Y en 
a marre. On peut plus sortir de chez soi. C’est du harcèlement 
(rumeur d’approbation parmi les badauds)._ » Marcelle et Claude, 
82 ans, retraités, en souffrent aussi : « _Depuis deux mois, 
c’est systématique, un journaliste nous arrête entre notre 
appartement et notre club de bridge. Ça fait dix-sept fois, 
dix-sept ! Pareil pour nos amis. On a tout essayé, changer de 
trajet, décaler nos horaires, ils nous retrouvent toujours. On 
réfléchit à prendre un pitbull._ » Pour Mariam, 36 ans, 
assistante comptable, ce n’est pas un problème : « _C’est vrai 
que depuis quelques temps on voit plus de journalistes, comme 
s’ils cherchaient quelque chose. Ils ne dérangent pas 
vraiment._ » D’autres apprécient le changement, comme Sofia, 
43 ans, puéricultrice : « _Moi je trouve ça bien, qu’on soit 
écoutés, ça montre qu’on se soucie de nous._ » Safouane, 16 ans, 
se réjouit : « _Avant j’étais jamais passé à la télé, et là ça 
fait trois fois. Mes potes sont trop jaloux !_ »

Les efforts finissent par payer : le 3 septembre, au 13H de TF1, 
un reportage sur les fournitures scolaires donne la parole à la 
rue. Soudain, au micro, beaucoup reconnaissent la voix qu’ils 
ont tant recherchée : égale à Elle-même et moyenne en tout, la 
passante idéale donne son avis. Mathis raconte : « _À partir de 
là, tout s’est accéléré. On avait son visage, on avait le lieu 
où TF1 l’avait trouvée._ » Les jours suivants, d’autres y 
retournent et, à force de sillonner le secteur, finissent par la 
croiser. Elle prolifère sur le petit écran.

Sans jamais connaître son nom — micro-trottoir oblige —, on la 
voit tous les jours sur une chaîne différente. De jour en jour, 
ses interventions esquissent le portrait d’une Française d’âge 
moyen et de profession intermédiaire. Les images montrent une 
femme au physique dans les normes et à l’habillement en général 
soigné, parfois à la mode, mais pas toujours. Issue d’une 
famille typique de la classe moyenne, son ménage est un cas 
d’école de la société française du vingt-et-unième siècle. 
Chacune des opinions qu’Elle exprime confirme les analyses 
précédentes : toujours nuancées, elles correspondent 
systématiquement à la moyenne des points de vue possibles 
pondérée par les résultats des derniers sondages Ifop. 
« _C’était trop beau. Il y en a même dans le métier qui ont cru 
à une sorte de canular. Et puis, un jour, tout s’est arrêté._ »

C’était le 15 octobre. « _Je zappe : sur TF1, rien, sur 
France 2, rien… Sur aucune chaîne, rien. À la radio, rien. Dans 
les journaux, rien._ » L’heure tourne et Mathis se rend 
à l’évidence : Elle n’a pas été micro-trottée ce jour-là. Une 
absence qui se prolonge les semaines suivantes. La raison de ce 
brusque silence ? « _Personne ne sait pourquoi, mais apparemment 
Elle a cessé de fréquenter ce quartier de Paris._ » Sa 
disparition plonge le microcosme journalistique dans le 
désarroi. « _Elle a chamboulé la profession, Elle s’est rendue 
indispensable, et puis Elle a disparu, comme ça. C’est très 
cruel. Ça laisse des traces._ » Sa voix décroche, Mathis 
détourne le regard.

10H15. Tifaine a fini avec le gros-là et revient vers nous. 
Soudain, Jérôme tend le nez. Ses poils sont dressés, ses 
pupilles dilatées : il a senti quelque chose. Il donne 
l’alerte : « _des Téfal, au Sud. Trois, peut-être quatre._ » Ses 
collègues se redressent sur leurs pattes arrières, en alerte. 
Ils hument l’air à leur tour. La menace se confirme. Les équipes 
de micro-trotting sont très territoriales : celle de Tifaine 
a soigneusement repéré et marqué la place depuis trois semaines, 
et elle est bien déterminée à la défendre. Maintenant prête pour 
le combat, elle se place à l’un des accès. Quelques minutes plus 
tard, trois journalistes de TF1 — des « _Téfal_ » dans l’argot 
de France Télévision — surgissent d’une rue transversale. 
Tifaine lance un grognement d’avertissement. La cheffe de la 
bande rivale réplique en relevant brusquement le front, en signe 
de défi. Bernard déclame alors les prouesses de son matériel 
— numérique 4K, cent-vingt images par seconde, correction de 
stabilité — pendant que Mathis déchire bruyamment des pages de 
son calepin. En face, la meneuse semble intimidée par cette 
démonstration de force et hésite à poursuivre la confrontation. 
Mais une jeune femelle se montre téméraire et franchit la ligne 
du trottoir. Aussitôt, le groupe de France 2 redouble de cris. 
Jérôme bondit et repousse l’intrus d’un coup de perche dans 
l’estomac. Le coup est sans gravité, mais la jeune Téfal 
a compris la leçon et recule prudemment parmi ses congénères. 
Son collègue s’approche pour lécher sa blessure. Avec un dernier 
reniflement de dédain, la cheffe Téfal s’éloigne en entraînant 
les siens. Si ces affrontements sont fréquents — d’autant plus 
depuis l’accroissement de la compétition sur les trottoirs de la 
capitale —, ils dégénèrent rarement. Le plus souvent, les chefs 
reconnaissent les prétentions de la meute la plus puissante.

10H50. L’alerte est passée. Les journalistes se détendent et 
retournent à leurs activités. Mathis explique : « _On sécurise 
ce terrain car il est très important pour nous._ » Les chaînes 
sont toujours à la poursuite de la passante idéale. « _À la 
rédaction, les experts en démographie ont classé la place 
Saint-Michel parmi les lieux où Elle aurait probablement pu 
trouver refuge._ » Sur quels éléments les experts fondent-ils 
leur jugement ? « _Il y a un point d’eau, beaucoup de nourriture 
à proximité, et une grande station de métro-RER. Et les ponts 
sont des passages stratégiques pour franchir la Seine._ »

Midi. L’équipe remballe son matériel. Aucun signe d’Elle, mais 
la chasse a été bonne malgré tout. Tifaine est satisfaite : 
« _On a trente-quatre interviews._ » Tout ne sera pas gardé. 
Certaines seront redondantes ; d’autres, pas assez télégéniques. 
Les homonymes seront systématiquement éliminés — clarté oblige. 
Bernard précise : « _J’ai presque quatre heures de rushes. La 
caméra tourne en permanence, comme ça on gagne du temps et on ne 
rate rien. Mais ça fait plus de boulot au montage._ » Il faut 
créer un équilibre entre les opinions. Les interrogatoires 
restants seront coupés et arrangés pour suivre la ligne 
éditoriale de la chaîne. Quelle est la direction voulue par 
France 2 ? « _Le représentatif. L’authentique. Le vrai._ »

Tifaine regrette : « _Évidemment, avec Elle, il n’y aurait pas 
besoin de couper. En fait, il n’y aurait même pas besoin 
d’autres interviewés ! C’est ce qui la rend si précieuse._ » La 
dernière, Tifaine saute dans la fourgonnette. « _Et maintenant, 
direction la rédac’ !_ »

~ Un reportage d’A. M. et J. G.

---

_**Note de l’auteur (24 février 2022).** Quand la réalité 
dépasse la fiction : [Roanne, ville moyenne où les citoyens 
votent exactement à l’image du pays][roanne] (encore en 2022)._

[roanne]: https://www.lemonde.fr/politique/article/2022/02/24/j-irai-voter-blanc-au-premier-tour-a-roanne-le-reflet-d-une-campagne-presidentielle-terne_6115013_823448.html

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
