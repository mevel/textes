---
date: 2018-11
summary: inspiré par un ancien thème de l’Atelier d’écriture, « Ping-pong »
...

# Un swing au Cocooning Blues

<style>
    .charline {
        font-family: "Comic Sans MS";
    }
</style>

_Dring !_ Je m’apprête à décrocher, puis j’ai un feeling 
étrange. Ça pourrait être du phishing ? _Driiing !_ Ou pire ? Un 
kidnapping ? _Dring dring !_ Les scènes de _Shining_ défilent 
dans ma tête. _Driiiiiing !_ Je hais Stephen King. 
_Driiiiiiiiing driiiiiiing !_ Je décroche. « Salut, c’est 
Charline. » Charline ! « Ça te dit, une soirée au Cocooning 
Blues ? » Un gros parpaing sur le cœur.

Le stress m’envahit. Je l’évacue sur mon punching-ball, mais 
j’arrête après qu’un looping ait envoyé valdinguer le vase Qing 
de mamie. Guère calmé, j’enfile mon jogging et sors faire un 
footing à toute berzingue. À deux pâtés de buildings, un groupe 
de hippies autour d’un camping-gaz fait un sitting devant le 
Burger King. Je passe en forcing, comme un viking dans le 
détroit de Béring. Enfin relax, je m’assois sur un banc.

Je fais un brainstorming. _Bon, le meeting est ce soir. Je dois 
établir un planning. Je gère._ J’ai toujours eu le sens du 
timing. Pas besoin de coach. D’abord, les fringues. Il faut que 
j’aie du style. Je fais un listing. Un grand bain. Du parfum. Un 
petit lifting. Ensuite, des fleurs. Un cadeau, peut-être ? Mais 
quoi ?

Après un détour par la banque (ING Direct) pour retirer du cash, 
je me lance dans une séance de shopping dans les buildings de 
grand standing du centre-ville, courant partout comme un lemming 
dingue. Je dépense sans compter : c’est un blockbuster et je 
suis dans le casting. Chez Hasting’s, j’achète un joli nœud 
papillon, et un smoking que je dépose au pressing. J’insiste 
bien sur l’urgence de la situation.

Chez le fleuriste, je choisis un bouquet de jonquilles que je 
lui demande de garder au frais.

À la parfumerie, les mannequins me narguent du haut de leur 
bodybuilding.

Chez moi, je prends une douche ultra-speed. Shampoing. Pas de 
temps pour le lifting. Une touche de parfum : _Darling_. À la 
meringue.

Un crochet par le pressing, que je trouve en plein rush. La 
blanchisseuse qui me tend mon smoking a l’air surbookée. _Elle 
devrait se mana… euh, se ménager, sinon elle risque le burn-out. 
Je sais de quoi je parle._

Sur le parking, je checke mon brushing et rajuste mon smoking. 
Je n’aurais pas déplu à Ian Fleming, j’ai l’air de sortir tout 
droit d’un de ses best-sellers. _Ça ne fait pas trop 
bling-bling ?_ Mon pacemaker joue au ping-pong. Je mâche un 
chewing-gum pour me booster (et puis, comme ça, pas d’haleine 
qui schlingue). Il est temps de monter sur le ring. C’est moi le 
King. Je pousse les portes du Cocooning Blues. _Ding, dong._

« Ouah, le look de steward ! Descends de ton Boeing, c’est un 
bowling, pas un shooting. » Je tombe de haut. Charline est déjà 
là, fidèle à elle-même, avec son éternel piercing au nombril et 
son string qui dépasse du jeans. Je me sens soudain ridicule, un 
grand bringue dans un déguisement, un vrai épouvantail 
d’Halloween. _Zut._

On s’installe à une petite table dans un coin cosy. Elle prend 
le pudding et moi la tartelette aux coings. Je peux sentir son 
parfum (à la tarte).

  On prend place dans une alcôve feutrée. Il prend une tarte 
  meringuée et moi du flan. Je peux sentir sa tarte.
  {: .charline }

On commence la partie. Elle enchaîne les strikes. Je ne fais pas 
mieux qu’un spare, une fois. 
« C’est du bowling, pas du curling ! » pouffe-t-elle.

_To be continued!_

<!-- « C’est un bowling, pas un dancing… » -->

# vim: set tw=64 et ts=2 fo+=taw:
