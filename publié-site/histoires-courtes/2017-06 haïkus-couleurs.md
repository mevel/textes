---
date: 2017-06
summary: quelques haïkus colorés en ma qualité de Consultant Couleurs
title: Haïkouleurs
...

*juin :*

. tout rayonne orange
. air liquide et nuit blanche
. aux feux du solstice

*juillet :*

. patriote au pas
. azur brûlant, blanche écume
. lire un livre à l’ombre

*août :*

. mettre table à l’ombre
. amis, melon, c’est sucré
. goûter l’insouciance.

*septembre :*

. sissille la cigale
. soudain, le silence. champ d’or
. mais le son, déjà.

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
