---
date: 2020-09-28
summary: sur le serveur Discord de la Med, minuit sonnant
...

# La police du sommeil

.
. alors que minuit sonne et qu’au cœur des ténèbres
. nous quittons un lundi pour saluer un mardi
. alors que l’Insomnie veut nos rêves maudits
. à l’heure où les loups hurlent une plainte funèbre

.
. entendez l’injonction : vos paupières sont lourdes
. regagnez tous vos lits et dormez des deux yeux
. avant que l’aube poigne et qu’un jour merveilleux
. et nouveau se révèle ; inspirez donc ma poudre !

_le matin suivant :_

.
. Debout debout, le Soleil brille
. La rosée perle et l'oiseau chante
. La froide nuit prend la tangente
. Debout debout, reprends ta vie !

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
