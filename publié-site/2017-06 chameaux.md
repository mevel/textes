---
date: 2017-06
summary: sur la mailing-list de la Lika
...

# Les chameaux

## Le sage

Un sage rencontre trois hommes dans le désert. Les voyant 
perplexes, il dit :

— Salut à vous, jeunes hommes.
— Salut à toi, vieil homme.
— Je vous vois là et vous semblez perplexes.
— C’est que, répond l’un, notre père est mort.
— Il est mort, poursuit un autre, a nous a légué ses chameaux.
— Les dix‐sept chameaux de son troupeau, précise le troisième.
— La moitié pour moi qui suis l’aîné.
— Le tiers pour moi qui suis le cadet.
— Et le neuvième pour moi qui suis le benjamin.
— Eh bien c’est simple, dit le sage, pourquoi ne partagez‐vous 
  pas le troupeau ainsi ?
— C’est que, la moitié de ces dix‐sept chameaux, cela ne tombe 
  pas juste.
— Le tiers de ces dix‐sept chameaux, cela ne tombe pas bien.
— Le neuvième de ces dix‐sept chameaux, cela ne tombe pas rond.
— Quelque chose ne tourne pas rond.

Le sage pense un instant, puis dit :

— Ah, je vois maintenant votre ennui. C’est que dix‐sept n’est 
  pas multiple de deux, ni de trois, ni de neuf.
— En effet, c’est tout à fait notre problème.
— Mais regardez là, voici mon chameau. Permettez que je l’ajoute 
  aux vôtres. Je l’offre à votre père.
— Nous ne pouvons pas l’accepter !
— Que deviendriez‐vous ?
— À pied dans le désert !
— C’est trop aimable à vous, jeunes gens, de vous soucier d’un 
  humble vieil homme. Voilà maintenant dix‐huit chameaux pour 
  votre héritage. Toi qui es l’aîné, prends‐en neuf, voici ta 
  moitié.
— Nous insistons…
— Toi qui es le cadet, prends‐en six, voici ton tiers.
— Nous ne pouvons…
— Toi qui es le benjamin, prends‐en deux, voici ton neuvième.
— Vous nous…
— Il en reste un, c’est le mien, je le récupère et je poursuis 
  mon chemin.

## Le zouave

Un zouave rencontre trois hommes dans le désert. Les voyant 
perplexes, il dit :

— Salut à vous, jeunes hommes.
— Salut à toi, soldat.
— Je vous vois là et vous semblez perplexes.
— C’est que, répond l’un, notre père est mort.
— Il est mort, poursuit un autre, a nous a légué ses chameaux.
— Les dix‐sept chameaux de son troupeau, précise le troisième.
— La moitié pour moi qui suis l’aîné.
— Le tiers pour moi qui suis le cadet.
— Et le neuvième pour moi qui suis le benjamin.
— Eh bien c’est simple, dit le zouave, pourquoi ne partagez‐vous 
  pas le troupeau ainsi ?
— C’est que, la moitié de ces dix‐sept chameaux, cela ne tombe 
  pas juste.
— Le tiers de ces dix‐sept chameaux, cela ne tombe pas bien.
— Le neuvième de ces dix‐sept chameaux, cela ne tombe pas rond.
— Quelque chose ne tourne pas rond.

Le zouave pense un instant, puis dit :

— Ah, je vois maintenant votre ennui. C’est que dix‐sept n’est 
  pas multiple de deux, ni de trois, ni de neuf.
— En effet, c’est tout à fait notre problème.
— Mais regardez là, voici mon couteau. Permettez que je partage 
  vos chameaux. Commençons par le père.
— Nous ne pouvons pas l’accepter !
— Que deviendrions‐nous ?
— Sans le reproducteur !
— C’est trop doux à vous, jeunes gens, de vous soucier de 
  chameaux. Voilà maintenant dix‐huit morceaux pour votre 
  héritage. Toi qui es l’aîné, prends‐en neuf, voici ta moitié.
— Nous insistons…
— Toi qui es le cadet, prends‐en six, voici ton tiers.
— Nous ne pouvons…
— Toi qui es le benjamin, prends‐en deux, voici ton neuvième.
— Vous nous…
— Le dernier morceau, je vous en débarrasse.
— Mais…
— Il reste seize chameaux, y a encore du boulot, je reprends mon 
  couteau et je poursuis la boucherie.

## Le mage

Un mage rencontre trois hommes dans le désert. Les voyant 
perplexes, il dit :

— Salut à vous, jeunes hommes.
— Salut à toi, sorcier.
— Je vous vois là et vous semblez perplexes.
— C’est que, répond l’un, notre père est mort.
— Il est mort, poursuit un autre, a nous a légué ses chameaux.
— Les dix‐sept chameaux de son troupeau, précise le troisième.
— La moitié pour moi qui suis l’aîné.
— Le tiers pour moi qui suis le cadet.
— Et le neuvième pour moi qui suis le benjamin.
— Eh bien c’est simple, dit le mage, pourquoi ne partagez‐vous 
  pas le troupeau ainsi ?
— C’est que, la moitié de ces dix‐sept chameaux, cela ne tombe 
  pas juste.
— Le tiers…
— Oui bon ça va, j’ai compris. Regardez là, voici mon chapeau. 
  Permettez que je me découvre. Pour honorer votre père.
— Nous ne pouvons pas l’accepter !
— Que deviendriez‐vous ?
— Nu‐tête dans le désert !
— Pas la peine d’en faire un fromage, jeunes sots. Chapeau, 
  combien font le demi de l’aîné plus le tiers du cadet ?
— Cela fait cinq sixièmes, mon bon mage.
— Chapeau, combien font ces cinq sixièmes et le neuvième du 
  benjamin ?
— Cela fait dix‐sept dix‐huitièmes, mon juste mage.
— Chapeau, cela fait‐il un ?
— Oh que non, mon rond mage.
— Voilà : ça ne fait pas un, c’était pas malin. Je me recoiffe 
  et je poursuis mon chemin.

# vim: set tw=64 et ts=2 fo+=taw:
