---
date: 2017-10-03
summary: 
    improvisé en réponse à une question puis posté sur la mailing-list de la Lika
    (« pourquoi », une question à s’arracher les cheveux)
...

# Pourquoi ?

De tout temps l’Homme s’est demandé « pourquoi ». Pourquoi la 
pluie ? Pourquoi le vent ? Pourquoi les feuilles qui tombent à 
l’automne ? Ces questions, chacun se les a un jour posées. Cette 
recherche de la compréhension, cet idéal des causes et des 
conséquences, sont peut‐être le reflet d’une quête de sens : 
l’Homme, au centre de son Univers absurde, est perdu. Il doute. 
Comment, dès lors, ne pas projeter ses doutes sur la Nature qui 
l’entoure ? Comment ne pas croire que cette Nature, ses Objets 
et ses Lois physiques, ne soient eux‐mêmes que la projection 
visible d’un Univers infiniment plus vaste ?

L’Homme, la Nature, et l’Univers. Ces questionnements 
vertigineux, ces perspectives de dimension supérieure, le 
botaniste Carl von Linné les a examinés et retournés sa vie 
durant. Mais la genèse d’une existence de doute, le point de 
mire d’une scrutation de l’infini, le terrier du lapin blanc
--- pourrait‐on dire ---, est tout entier contenu dans un fait 
divers infime : au sortir de l’université de Stockholm, par un 
soir d’octobre 1853, le jeune Carl

voit tomber

une feuille

_

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
