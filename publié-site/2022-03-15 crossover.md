---
date: 2022-03-15
finished: 2022-04-17
summary:
    romance et crossovers improbables, 
    le monde étrange et merveilleux de la fanfiction
title: Ma première fanfiction
...

# Hunger Games : Expiation
# (ou : Ma Première Fanfiction)

<style>
    /* Affiche les répliques sans italique par défaut, 
       pour qu’on reconnaisse les flashbacks qui eux sont 
       en italique. */
    p.dialog-line { font-style: normal; }
</style>

Était-ce fini ? Il avait du mal à le croire. Il se tenait en 
retrait sur un haut balcon. De la place monumentale devant lui 
s’élevait une clameur assourdissante. Il lui semblait bien que 
le spectacle continuait.

Un homme aux cheveux blancs, en costume officiel, lui tournait 
le dos. Il pouvait facilement lui sauter dessus et l’étrangler, 
l’égorger avec ce qui lui tomberait sous la main, ou bien lui 
tordre le cou. Son adversaire n’avait pas l’air bien solide 
— d’un autre côté, lui-même respirait avec difficulté, et ses 
mouvements restaient gauches dans la flambante cuirasse qui 
enserrait son corps meurtri. Le plus sûr était peut-être de 
jeter le vieillard dans le vide.

Était-ce ce qu’on attendait de lui ? Qu’il tuât encore ?

Le vacarme le désorientait. Alors qu’il hésitait, il revit une 
autre tribune depuis laquelle, dans une autre vie, il avait 
affronté une autre foule. Quel contraste entre les vivats de 
tous ces dandys pomponnés et les visages fermés des ouvriers 
silencieux…

Il s’efforçait de rester à l’affût, concentré sur le présent 
comme il n’avait pas cessé de l’être depuis plusieurs jours, 
mais la fatigue accumulée, la violence des sentiments qui se 
déchaînaient toujours en lui, étaient sur le point de le 
terrasser. Il n’était plus certain d’être cohérent. Son esprit 
s’évadait.

_Son ticket de sortie… Depuis tout petit il rêvait de s’échapper 
de la Colonie 6. Il ne pouvait pas s’imaginer une vie entière 
à trimer comme un esclave dans le sable et les carcasses 
métalliques. Il détestait ce désert qui s’insinuait partout et 
grippait tout, hommes comme machines ; ce désert qui tuait sa 
mère à petit feu. Loin de ce fléau s’étendaient les autres 
colonies et les merveilles du Capitole…_

_Il ne le savait que trop bien, c’était sans espoir. Il n’y 
avait guère qu’une seule façon de s’arracher à la Colonie. 
Aussi, quand l’assesseuse de la Moisson avait prononcé son nom 
et qu’il s’était avancé, hébété, entre ses compatriotes, une 
seule pensée s’était fait jour dans son esprit : ce bout de 
papier tiré de l’urne et qui portait son nom, c’était son ticket 
de sortie. À peine avait-il entendu sa mère hoqueter un sanglot. 
L’adolescent n’avait pas encore accusé toute la gravité de sa 
situation. Que pouvait-il y avoir de si terrible là-dedans ? Les 
courses de bolides auxquelles il concourait avec témérité 
étaient, elles aussi, d’une violence sauvage. Sa mère et Pad lui 
reprochaient sans cesse son inconscience, pourtant il était 
toujours vivant, n’est-ce pas ? Il en remontrait même 
à Furiosa._

_Oui, s’était-il encouragé en montant sur l’estrade, il 
gagnerait les Jeux de la Faim et sa place dans l’univers. Il 
emmènerait Pad avec lui. Ils parlaient souvent de partir 
ensemble. C’était elle qu’il regardait en déclamant la phrase 
rituelle :_

— _Soyez témoins !_

Folie, songea-t-il tandis que le vieil homme devant lui levait 
la main et que la foule se taisait. C’était une leçon qu’il 
avait apprise amèrement : personne ne gagne jamais les Jeux. 
Personne.

_La réalité l’avait rattrapé un instant plus tard, quand 
l’hôtesse avait proclamé un second prénom, féminin. Pad. Il 
avait voulu croire avoir mal entendu, mais alors il avait vu le 
choc céder à l’horreur sur le visage de son amie. Le regard 
qu’ils s’étaient échangé avait tout dit de leur égarement, de 
leur désespoir. Ils ne pouvaient plus dorénavant rêver d’évasion 
heureuse. La règle des Jeux était implacable : nécessairement, 
l’un d’entre eux devrait mourir._

— … Car la vie, dans tout l’Empire, n’est possible que grâce 
  à la générosité du Capitole. Souvenons-nous de…

L’officiel prononçait un discours auquel le jeune homme avait du 
mal à donner un sens, comme à tout le reste, mais le thème était 
clair : il rappelait la gloire du Capitole. La voix amplifiée se 
réverbérait d’un bout à l’autre de la place immense entre les 
volumes démesurés des façades gouvernementales. Des mots 
à l’architecture, tout soulignait, appuyait la puissance 
écrasante de la capitale.

_Le luxe… Le luxe de la navette qui les conduisait au Capitole 
était obscène mais les boiseries, les tapis, les lampes, les 
radiateurs discrets, même les rutilantes finitions de laiton 
échouaient à capter son attention. Tout cela, l’adolescent ne 
l’apercevait que confusément. Il avait l’esprit ailleurs. Assis 
devant une délicate table en acajou, Pad et lui attendaient en 
silence._

_Une porte automatique s’était ouverte sur une femme qui 
détonnait dans le décor impeccable et feutré. Même lavée et 
vêtue avec un soin inhabituel, sa présence véhiculait sinon 
l’odeur, du moins l’idée de la sueur, de la poussière et de 
l’huile de moteur. Sa prothèse de bras, tout de graisse et de 
rouille, n’arrangeait pas les choses. Furiosa avait déboulé dans 
le compartiment l’allure plus massacrante que jamais._

— _Alors, qu’est-ce qu’on a cette année ? Tiens tiens…_

_Elle plissait les yeux devant lui._

— _Toi, je te connais. T’es le gamin qui s’obstine à vouloir me 
  griller à la course. Suicidaire. Mais têtu, ça, c’est bien. Et 
  toi…_

_Elle avait reporté son attention sur la jeune fille._

— _Bon, toi, t’as l’air d’avoir du plomb dans la cervelle. Va 
  falloir bosser le physique, et t’auras ptet une chance de 
  survivre._

_Devant leur air absent, les traits de Furiosa s’étaient 
brièvement adoucis, esquissant une expression que le garçon ne 
lui avait jamais vue quand elle surplombait les autres 
chauffards depuis la cabine de son _truck_ monstrueux._

— _Écoutez, dans cet univers, tout n’est que douleur. Vous 
  voulez vous en tirer ? Faites ce que je vous dis._

Il obéit mécaniquement à l’injonction de l’orateur qui lui 
faisait signe d’avancer. Ses nouvelles jambes de métal — qu’il 
ne sentait pas — le portèrent au bord du balcon, face à la foule 
innombrable loin en contrebas. C’était une masse informe et 
vague. Quelles menaces, quels ennemis recelait-elle ? Il 
n’aurait su le dire.

_Une minute seulement. Il scrutait tour à tour les autres jeunes 
gens, perchés comme lui sur des socles disposés en cercle. 
Certains le dévisageaient en retour, d’autres fixaient un 
objectif comme s’ils pouvaient l’attraper par la seule force de 
leur concentration. Tous se tendaient, prêts à bondir. Tous 
étaient ses ennemis. Il jaugeait le danger qu’ils 
représentaient, s’efforçait de deviner leurs intentions. Trente 
secondes. Il avait trouvé le regard de Pad. Selon les règles, 
elle aussi était son ennemie. « Vingt-quatre entrent, un seul 
sort. » Il refusait de la voir ainsi. Quinze secondes. Il avait 
reporté son attention sur les objets étalés dans le cercle. Sacs 
à dos, barres céréales, pansements. Glaives. Shurikens. Arcs et 
flèches. Poignards à lancer. Couteaux crantés. Machettes. 
Haches. Battes de baseball. Lacets étrangleurs. Pièges à loup. 
« Ramassez ce que vous pouvez et courez. » Le conseil de Furiosa 
résonnait dans son esprit. Son cœur battait à tout rompre._

_Le compte à rebours avait atteint zéro._

C’était avec une lenteur délibérée, théâtrale, que le vieillard, 
sur sa gauche, saisissait un objet brillant sur un coussin qu’on 
lui présentait. Qu’est-ce que c’était ? Il faisait toujours face 
à la foule et l’oppressant heaume dont on l’avait affublé gênait 
sa vision périphérique. Il se crispa. Si c’était une arme…

_La pointe de la lame piquait toujours son abdomen. Il retenait 
son souffle, ses yeux plongés dans ceux de Pad. La main de la 
jeune fille tremblait. Soudain, elle avait lâché le couteau._

_L’arme n’avait pas plus tôt touché terre qu’il s’était 
précipité contre elle. Il l’avait serrée longtemps, le menton 
sur son épaule, afin qu’elle ne pût pas voir ses larmes. Elle 
non plus ne semblait pas vouloir le lâcher._

— _Jamais, jamais…_

_Quand ils s’étaient finalement séparés, il avait vite détourné 
le regard._

— _Montre-moi ta jambe._
— _Laisse-moi. Je serais un boulet pour toi. Il faut que tu 
  vives._
— _Pas question. Pas sans toi. Ta jambe._

_Allongée sur la pierre, visiblement à bout de forces, elle 
s’était résignée à le laisser examiner la profonde entaille. 
À l’aide du couteau ramassé, il avait arraché la jambe de la 
tunique de Pad pour dégager la blessure, avait hâtivement taillé 
une bande dans le tissu isotherme et l’avait nouée autour de sa 
cuisse le plus délicatement qu’il avait pu. Il espérait que ce 
garrot improvisé suffirait à bloquer l’hémorragie ; il ne 
pouvait pas faire mieux. Avec les lambeaux de la manche, il 
avait ensuite épongé le sang, nettoyé la plaie à l’eau douce 
— cette grotte était une bénédiction — et confectionné un 
bandage grossier._

_Sa tâche achevée, l’épuisement l’avait gagné et il s’était 
enfin autorisé à s’écrouler au côté de Pad. Les deux adolescents 
s’étaient pris la main en silence, comme un serment muet._

Il pivota vers l’orateur, qui s’approchait avec l’objet comme 
s’il voulait lui en faire présent. C’était une sorte de grand 
anneau ouvragé dans un métal doré. Une couronne. Le jeune homme 
était de plus en plus perplexe. Ça ne pouvait pas être pour lui. 
Il ne la méritait pas. Il avait trahi Pad.

_Un son clair, incongrûment musical, l’avait tiré de sa 
somnolence au petit matin. Pad s’était assoupie contre lui. Sa 
poitrine se soulevait à un rythme régulier. Il était resté 
décontenancé un long moment avant de reconnaître tout à coup le 
carillon d’un drone livreur. Un don des sponsors ! Au bruit, il 
devait être tombé tout près. Il leur était sans doute destiné ; 
peut-être de quoi soigner la jambe de Pad ? Il avait contemplé 
son amie, et avait été saisi par l’envie de l’enlacer 
à nouveau._

_Au lieu de ça, il s’était dégagé en tâchant de ne pas la 
réveiller. Il avait hésité à la laisser mais, outre qu’elle 
avait besoin de soins, il devait faire taire le drone avant 
qu’il n’attirât quelqu’un d’autre. Depuis combien de temps 
sonnait-il, d’ailleurs ? De toute façon il ne s’éloignerait 
guère de la grotte._

Alors que le politicien face à lui levait la couronne dans 
l’éclat du Soleil puis la déposait sur son casque, il acquit la 
conviction qu’il s’agissait d’un nouveau cadeau empoisonné.

_C’était en brandissant avec enthousiasme la boîte de pommade 
qu’il avait pénétré dans la grotte. En un éclair il avait 
découvert son amie redressée, son visage livide dans la 
pénombre, ses yeux inexpressifs et qui ne le voyaient plus ; la 
silhouette sombre penchée sur elle ; entre eux, l’éclat argenté 
qu’avait capté, à la faveur d’un mouvement de torsion imprimé 
par l’intrus, la longue, longue lame du sabre qui transperçait 
Pad. Une tache rougeâtre s’élargissait sur le torse de la jeune 
fille. Du sang épais mêlé de salive avait jailli de ses lèvres 
et coulé sur son cou pâle. Puis elle s’était effondrée. Coup de 
canon._

Les tambours tonnèrent. Un roulement grave, puissant, qui fit 
vibrer son plastron et que la liesse qui se déchaîna ne parvint 
pas à couvrir complètement. L’officiel avait saisi son poignet 
et l’avait brandi bien haut face à la foule. Il ne ressentait 
plus rien.

_La fille se démenait frénétiquement à quatre pattes pour lui 
échapper. Elle semblait à peine plus jeune que lui, quinze ans 
à peu près. Peut-être était-elle du groupe qui les avait pistés, 
Pad et lui. Peut-être pas. Elle vivait, Pad était morte ; il 
devait laver cette injustice. Le sang des deux autres, plus tôt, 
n’avait pas suffi à effacer celui de son amie sur la lame du 
sabre. Ses doigts s’étaient raffermis sur le manche. Il avait 
rattrapé la fuyarde en trois enjambées, lui avait plongé l’acier 
glacial dans le dos et l’en avait vivement retiré tandis qu’elle 
s’affalait dans les feuilles mortes. La lame était ressortie de 
la chair plus rouge que jamais._

_Il avait retourné sa victime et s’était accroupi pour la 
contempler. Elle écarquillait les yeux et son corps agité de 
tremblements fébriles luttait désespérément pour happer et 
expulser de l’air, trop peu, trop vite, avec un bruit de 
raclement. Il avait perforé un poumon. Ça prenait trop de 
temps : d’autres tributs vivaient toujours. D’un mouvement 
latéral de son sabre, il lui avait tranché la gorge. Coup de 
canon._

Le vieillard dressait toujours triomphalement le bras du jeune 
homme. C’était son bras véritable, celui de gauche. Son poing 
était serré.

_Coup de canon. Il avait lâché la grosse pierre ensanglantée et, 
comme une chose informe, s’était lourdement laissé tomber près 
du cadavre du colosse. Il avait fini par l’avoir, lui aussi, 
malgré les amputations successives que le tranchoir de son 
adversaire lui avait brutalement infligées. Une souffrance 
indescriptible, qui semblait irradier de partout à la fois, 
était sur le point de le submerger. Au moment où il allait 
sombrer, toutefois, il s’était souvenu qu’il restait un dernier 
tribut._

_Il s’était tortillé pour faire face au garçon recroquevillé 
dans un repli du rocher. Paralysé par la terreur, celui-ci 
n’avait pas songé à s’enfuir tandis que les deux adolescents 
s’entretuaient. Avec le bras seul qui lui restait, traction 
après traction, luttant contre l’évanouissement, le jeune homme 
avait traîné son tronc mutilé vers l’enfant. Il devait se 
presser, bientôt il n’en serait plus capable. Il ne fallait pas 
que le garçon lui échappât. Il empoignait de pleines mottes 
d’humus qui se désagrégeaient et lui filaient entre les doigts. 
Il s’était rappelé le sable de sa Colonie 6 ; une bouffée de 
rage avait décuplé ses efforts._

_Il avait finalement agrippé le corps frêle, l’avait précipité 
à terre il-ne-savait-comment et s’était abattu sur lui de tout 
son poids pour l’immobiliser. Sa main s’était emparée du petit 
cou de son ennemi. L’enfant s’était débattu mais il l’avait 
maîtrisé en heurtant son crâne juvénile contre la roche. 
À présent il comprimait la trachée de toute sa force. Ben. Le 
garçon s’appelait Ben, s’était-il souvenu. Pad l’aimait bien. Il 
pressait toujours. Cela avait duré un long moment. Il n’était 
plus que ce poing serré, cet étau d’acier. Le reste s’effaçait ; 
à peine avait-il entendu le coup de canon. Il avait continué 
à serrer, serrer._

Il fut soulagé quand on le laissa enfin baisser son bras, mais 
ce fut pour se voir infliger une poignée de main vigoureuse et 
une étreinte paternelle. D’une voix rauque, l’Empereur Snow lui 
confia :

— Il y a beaucoup de force et de colère en toi. Tu pourrais tant 
  faire pour l’Empire, Anakin.

---

_**Note de l’auteur.** Vous en voulez plus ? L’idée initiale, 
dans la droite lignée des fanfictions sentimentales et des 
crossovers improbables, était de raconter la relation tragique 
(et un brin sadomasochiste) entre le Geai Moqueur et Dark Vador, 
s’entremêlant au destin de l’Empire. Le texte ci-dessus en 
constituerait le prologue, qui permettrait de construire 
l’antagoniste masculin puisque le reste de l’Œuvre serait narré 
du point de vue de l’héroïne. Hélas, mille fois hélas, si 
l’existence d’une telle Œuvre me ferait toujours bien marrer, je 
n’ai en revanche guère envie de me farcir la rédaction de ses 
nombreux chapitres. J’ai (hélas) d’autres chats à fouetter. Avis 
aux plumes amatrices…_

<!--------------------------------------------------------------

BROUILLON PROLOGUE:

flashbacks:
+ A Moisson lui
+ B Moisson Pad
+ C scène avec leur mentor Furiosa
+ D début des Jeux, les tributs se regardent, hagards
+ E scène tragique romantique avec Pad
  + Pad est blessée, ils s’embrassent, romance tragique, etc.
+ F ils sont allongés côte à côte;
    sonnerie drone cadeau, Anakin va voir
+ G quand il revient, il découvre qqun en train de la poignarder
+ H tuerie sanglante:
    sabre rouge (de sang), celui qui a tué Pad
    laver la lame du sang de Pad avec celui des autres
+ I tuerie sanglante:
    Anakin étrangle un enfant avec le poing qu’il lui reste
    le dernier survivant; ~12 ans, "Ben", Pad l’aimait bien;
    Anakin a perdu l’avant-bras gauche et ses 2 jambes 
    (contre le protecteur de l’enfant ?)
    l’enfant à terre se débat mais Anakin tient bon 
    en pesant sur lui de tout son poids, 
    ça dure longtemps, longtemps, 
    il ne le lâche pas même quand retentit le canon

étapes du présent:
+ 0 Snow est devant lui sur un haut balcon devant une foule 
  immense, clameur abrutissante
  + Anakin est derrière le podium, entend mais ne voit pas la 
    foule, il apparaîtra publiquement au balcon quand Snow le 
    fera s’avancer
+ 1 Snow fait signe, la foule se tait, il entame un discours
+ 2 fin du discours, nouvelles clameurs
+ 3 Snow fait signe derrière lui pour appeler Anakin (et coussin)
  + Anakin s’avance mécaniquement
+ 4 Snow prend couronne sur coussin
+ 5 Snow s’approche avec couronne
+ 6 Snow donne couronne
~ 7 Snow brandit poing d’Anakin (son bras gauche, biologique)
~ 8 …
~ 9 Snow parle

à caser:
+ “I don't like sand. It's coarse and rough and irritating and 
  it gets everywhere.”
  « Je n’aime pas le sable. Il est grossier… agressif, irritant. 
  Il s’insinue partout. »

fausse piste Mad Max:
+ sable, carcasses rouillées
+ courses de bolides sauvages et violentes
- la V8 Interceptor
- pénurie de pétrole
- (Mad Max 3:) (au-delà du) dôme de tonnerre = l’arène
+ (Mad Max 3:) "deux hommes entrent, un homme sort"
+ (Mad Max 4:) "Soyez témoins !"
- (Mad Max 4:) les "War Boys"
+ (Mad Max 4:) Furiosa
  + Furiosa = ancienne gagnante des Jeux… et donc mentor 
    d’Anakin et Pad !
  + bras gauche = moignon, prothèse
  - borgne (fin du film)

prénom pour l’assesseuse:
* Decima ? https://fr.wikipedia.org/wiki/Parques ? <--
* Juventa ? https://fr.wikipedia.org/wiki/Juventas
  déesse de la jeunesse, associée au passage à l’âge adulte

BROUILLON GÉNÉRAL:

crossover improbable: Hunger Games + Star Wars (+ Harry Potter?)
ou alors Nausicaä de la Vallée du vent ! ça colle mieux :-)

relation Hermione / Vador

districts ~~> planètes (ou "colonies" ?)
District 6 ~~> Colonie 6 = Tatooine
Capitole <~~ Coruscant
Panem ~~> l’Empire (le Pan-Empire ?)
Empereur Snow <~~ empereur Palpatine <~~ Lord Voldemort
maître des Jeux ~~> Lando Calrissian

persos:
- Anakin de la planète Tatooine,
  devenu le sanguinaire Seigneur Vador servant l’Empereur Snow
  + passé: vainqueur des Jeux de la Faim il y a ~10 ans;
    son co-tribut était Padmé ("Pad"? "Ami"?) son amoureuse;
    sa mort tragique l’a rendu fou de chagrin et de haine, il 
    s’est lancé dans une fureur sanglante qui l’a fait gagner et 
    a suscité l’intérêt de l’Empereur
    => prologue
  + devenu le sinistre chef des Pacificateurs
  + porte un casque intégral ("heaume") car défiguré lors des 
    Jeux et un costume noir à cape
  + arme fétiche = sabre à vibration laser qui tranche comme dans du beurre 
    (car c’est avec un sabre qu’il a gagné les Jeux)
    ou alors baguette magique ?
  + pouvoirs psychiques = peut étrangler à distance
  + jaloux de Hermione + Ron car ils ont pu survivre aux Jeux 
    à 2 alors qu’il n’a pas eu cette chance avec Padmé
- Hermione de la planète Gryffondor(?)
  + brillante mais aussi très courageuse et déterminée à vivre
  + virtuose du sabre grâce à son expérience de la chasse sur sa planète natale
- Haymitch <~~ Sirius Black / Remus Lupin

arc ~~> sabre laser <~~ baguette magique
baguette laser ?

scénario:
Hermione est tribut aux Jeux cette année.
Volontaire, pour sauver XXX.
Après beaucoup de mélo, elle gagne.
Son attitude rebelle provoquera une révolte dont elle deviendra une meneuse et le symbole.
Vador sera donc amené à la combattre…

Ultimement Vador se retournera contre Snow qui lui ordonnait de tuer Hermione.

titre:
- Anger Games : Au-delà du dôme de colère
- Hunger Games : Expiation <--

-->

# vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—:
