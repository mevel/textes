---
date: 2017-10-29
summary: 
    envoyé à la mailing-list de la Sauce, journal étudiant de 
    l’ENS Cachan ; il ne faut pas s’étonner du résultat quand 
    tous les numéros comportent la mention « Envie de nous 
    envoyer un article, un dessin, un poème, un ornithorynque ou 
    une déclaration d’amour ? Une seule adresse […] »
...

# L’ornithorynque

.
. L’apathique ornithorynque
. à la tombée des brouillards
. quand cancane le canard
. ne quitte pas son plumard.

.
. L’apathique ornithorynque
. dans les froids de février
. quand s’empoudre l’oie cendrée
. hiverne dans son terrier.

.
. L’apathique ornithorynque
. dans l’air lourd d’odeurs fleuries
. quand pépie le colibri
. barricade son abri.

.
. L’apathique ornithorynque
. au temps du soleil de plomb
. où plane haut le faucon
. ne sort pas de son salon.

.
. Si c’est un tel cassos
. c’est qu’à toutes les Sauces
. c’est de lui qu’on se gausse
. c’est toujours lui qui trinque.

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
