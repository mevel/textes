---
date: 2010-08
summary: vacances en Islande,
    caser les mots « <span class="spoiler">banane, ceinture, 
    cheminée, marteau, dauphin</span> »
...

# Bingo Bozo

L’homme entra sur l’invitation de la voix. La pièce était 
éclairée et chauffée par une antique cheminée de pierre dans 
laquelle rougeoyaient des braises. Un fauteuil faisait face au 
foyer ; à en juger par son épaisseur et sa proximité du foyer, 
ce devait être une place confortable pour y lire, le soir.

À l’instant où Friedrich refermait la porte avec un léger bruit, 
le fauteuil pivota en un lent demi-tour.

— Je vous attendais, professeur Schmultz.

Ces paroles, énoncées d’une voix métallique et implacable, 
avaient soudain surgies du fauteuil. L’être qui y était assis 
était visible désormais. Avec son long nez fin et ses yeux 
rieurs, il évoquait à Friedrich, bien qu’il n’en ait jamais vu, 
un dauphin. Cependant, une lueur maléfique semblait briller au 
fond de ses yeux et sa bouche se tordait en un affreux rictus de 
triomphe. Il portait accroché à sa ceinture, sur son côté droit, 
un marteau effilé. Friedrich sentit son sang se glacer lorsqu’il 
vit, à sa gauche, la poche-revolver vide et, surtout, la banane 
qui devait y loger pointée sur sa poitrine.

— Que voulez-vous ? s’enquit-il, en maîtrisant tant bien que mal 
  le tremblement de sa voix.
— Vous le savez aussi bien que moi, lui répondit Bozo le Clown 
  — Car c’était lui — en élargissant son sourire au-delà du 
  possible. Où est la clef ?

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
