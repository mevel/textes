---
date: 2017-11-22
summary: atelier d’écriture, thème « tout et son contraire »
...

# Tout et son contraire

.
. C’est un taudis lugubre accroché sur le flanc
. de la falaise amère uniformément grise.
. Dans ce pays stérile écrasé par les trombes
. pousse sur son rocher un lichen entêté :
. la cabane improbable où je me réfugie.

.
. La grotte originelle, autrefois cache à rhum,
. se perd sous un lacis d’extensions verticales.
. Aujourd’hui l’on y met les réserves de fioul
. pour le réseau central qui par bien des détours
. irrigue chaque salle en chaleur salvatrice.

.
. Son squelette animé par le feu de la vie,
. tout de vieux bois craquant et de fauteuils moelleux,
. c’est le phare espéré contre un dehors hostile.
. Le tambour de la pluie, le fracas de la mer.
. Ici tout participe à l’ambiante gaieté.

.
. Ici se concilient les éléments contraires.

.
. Plein de mélancolie, le palais déserté
. chante au soleil de plomb son silence lunaire.
. Sous l’ombre bienvenue des nombreux péristyles
. attend la pierre ocrée des couloirs anguleux
. du dédale où le deuil a trop souvent sévi.

.
. Cet air frais n’a pas su guérir l’impératrice
. lorsqu’un foyer de fièvre a consumé sa cour
. tandis que la famine éclaircissait les foules.
. Seul témoin, oublié, sereinement s’étale
. le palais impérial au discret décorum.

.
. Tu quittes volontiers l’impassible logis
. docilement guidé par le sentier côtier
. où les pins parasol agrémentent les combes.
. La plage bigarrée t’apparaît par surprise
. baignée par la lumière, ornée de cerf‐volants.

# vim: set tw=64 et ts=2 fo+=taw:
