---
date: 2021-09-20
summary: atelier d’écriture,
    thème « il suffisait d’un grain de sable »
    + thème précédent « une rencontre »
title: Le grain de sable
...

[Musique d’accompagnement suggérée.](https://www.youtube.com/watch?v=TxfJbu-z_0Q)

# Le grain de sable

<style>
    .ryu {
        margin-left: 6ex;
        text-align-last: right;
        text-indent: 0;
        /*font-style: italic;*/
        font-family: "Comic Sans MS";
    }
</style>

Ce matin, comme tous les matins, le réveil de Miyuki sonna 
à 6 H 30.

Ce matin, comme tous les matins, pas encore tout à fait 
réveillée, Miyuki picora la boule de riz gluant et but le bol de 
soupe miso que sa mère avait préparés pour elle la veille.

Ce matin, comme tous les matins, Miyuki se brossa les dents de 
7 H 11 à 7 H 14.

  Ce matin, comme tous les matins, Ryû se réveilla vers 6 H 30.
  {: .ryu }

  Ce matin, comme tous les matins, Ryû engloutit trois tartines 
  de confiture et descendit une grande tasse de café.
  {: .ryu }

  Ce matin, comme tous les matins, devant sa glace, Ryû passa 
  23 minutes à donner à ses mèches un air naturellement 
  décontracté à grands renforts de gel capillaire.
  {: .ryu }

Ce matin, comme tous les matins, Miyuki enfila son uniforme 
soigneusement plié, passa sa sacoche et sortit dans la rue 
encore humide de rosée.

  Ce matin, comme tous les matins, Ryû enfila son uniforme 
  froissé, son sac à dos déchiré et sortit dans la ruelle encore 
  brumeuse après la nuit.
  {: .ryu }

Ce matin, comme tous les matins de printemps, Miyuki emprunta 
l’allée de cerisiers. Comme tous les matins, Miyuki ferma les 
yeux rien qu’un instant pour respirer leur douce senteur.

  Ce matin, comme tous les matins, par un détour de la route que 
  prenait Ryû, le paysage s’ouvrit sur la ville escarpée et le 
  chantier naval en contrebas. Comme tous les matins, Ryû 
  s’égara dans ses rêveries en contemplant les petits bateaux 
  sur la mer au loin.
  {: .ryu }

Ce matin, comme tous les matins, Miyuki prit le métro de 7 H 42 
à la station Akasaka pour se rendre à son lycée dans le 6e 
district de Tokyo.

Ce matin, comme tous les matins, assise contre la vitre, Miyuki 
traça distraitement un cercle sur la buée en écoutant son 
baladeur.

  Ce matin, comme tous les matins, Ryû prit le train de 7 H 44 à 
  la station Gokokuji pour se rendre à son lycée dans le 3e 
  district de Tokyo.
  {: .ryu }

  Ce matin, comme tous les matins, en attendant son arrêt, Ryû 
  sortit son carnet et poursuivit ses croquis du port, en 
  s’efforçant de ne pas penser au dessin inachevé sur la 
  dernière page.
  {: .ryu }

Ce matin, comme tous les matins, Miyuki quitta son premier train 
à 7 H 56 et traversa la grande station pour aller attendre sa 
correspondance de 8 H 01.

Ce matin, comme tous les matins, Miyuki remarqua le garçon qui 
arrivait en face à grandes enjambées. Comme tous les matins, 
Miyuki se passa la main dans les cheveux en baissant les yeux.

Ce matin, comme tous les matins, Miyuki se demanda comment il 
s’appelait, d’où il venait. Elle savait seulement où il allait, 
grâce à son uniforme. Quelle chose étrange que la routine, qui 
lui faisait croiser sans cesse les mêmes inconnus.

  Ce matin, comme tous les matins, Ryû descendit à 7 H 57 et 
  rejoignit le quai de son second train, celui de 8 H 03.
  {: .ryu }

  Ce matin, comme tous les matins, en débouchant sur le quai, 
  Ryû leva les yeux et vit cette fille qui semblait perdue dans 
  ses pensées. Comme tous les matins, Ryû détourna son regard 
  avant de croiser le sien.
  {: .ryu }

  Ce matin, comme tous les matins, Ryû se concentra pour 
  mémoriser les traits de l’inconnue aperçue à la dérobée. Il 
  complétait son dessin peu à peu. Il connaissait pratiquement 
  son visage par cœur sauf, évidemment, ses yeux.
  {: .ryu }

Ce matin-là, à 8 H 01, le métro en direction de l’Ouest n’était 
pas arrivé. Miyuki leva le nez pour vérifier l’écran 
d’informations.

  Ce matin-là, à 8 H 01, le métro en direction de l’Ouest 
  n’était pas arrivé. Ryû risqua un regard vers le quai d’en 
  face.
  {: .ryu }

Ce matin-là, étonnée par ce retard non annoncé, Miyuki baissa la 
tête. Son regard croisa celui du garçon.

  Ce matin-là, son regard croisa celui de la fille.
  {: .ryu }

_À suivre… ? (lol non)_

# vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—:
