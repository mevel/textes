---
date: 2022-04-11
summary: atelier d’écriture, 
    décrire une habitation en essayant de véhiculer des émotions 
    ou une atmosphère en utilisant le moins d'actions possible 
    (exercice issu de <i>Noir sur blanc</i> par Mylène Fortin)
...

# Home sweet home

Deux grands sacs de sport se tiennent prêts dans un placard près 
de l’entrée. Celui sur l’étagère bée. En dépassent des cols de 
sacs poubelle garnis de chaussettes, de chemises et de 
pantalons.

Le couloir étroit donne sur un salon assez vaste, lumineux grâce 
à deux fenêtres hautes. Les murs sont nus.

Un bar délimite une cuisine. Les plaques et le plan de travail 
sont propres. Au-dessus s’empile de la vaisselle impersonnelle. 
Cependant, sur le bar trône fièrement une plante en pot 
— véritable.

L’espace central de la grande pièce est occupé par une douzaine 
de feuilles de papier étalées à même le parquet.

Dans un coin, là où sont regroupés tous les types de prises 
murales, s’appuie une table toute simple. Il y a un fauteuil, 
une petite valise d’où jaillit du matériel informatique, un 
écran en équilibre sur une pile de cartons d’emballage, une 
vieille assiette, un courrier laissé sur un bord. Par terre, un 
bac où l’on a jeté des papiers hétéroclites.

À l’autre bout, un lit défait. Par terre, à côté de trois livres 
de poche usés, quelques vêtements entassés pêle-mêle. 
À l’exception de quelques draps, l’armoire de vieux bois 
branlant qui se dresse non loin est vide.

Il n’y a pas d’autre meuble.

# vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—:
