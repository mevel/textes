---
date: 2021-10-25
finished: 2021-11-08
summary: atelier d’écriture, thème « Cordialement »
...

# Cordialement

. Objet: lettre d’amour

.
. Cher Administrateur
. chère Administratrice
. et cher législateur
. ou chère législatrice
. de mon cœur,

.
. Je vous écris ce jour
. je vous écris cette heure
. du lieu-dit de Senlis
. afin d’éventuellement
. solliciter d’avance
. de votre bienveillance
. un gentil document
. tout simplement.

.
. J’aimerais en effet
. et vous serais fort gré
. je souhaiterais savoir
. quel est le sentiment
. que vous pourriez avoir
. à mon égard.

.
. Ma lettre est motivée
. et je vous suis fort gré
. par le fait que moi-même
. d’après tous les barèmes
. ma personne est éprise
. de… je vous aime.

.
. S’il y avait méprise
. sur la destination
. j’aimerais m’excuser
. faites-le moi savoir
. je vous en serais gré
. au dos de l’accusé
. de réception.

.
. Je veux vous assurer
. de ma motivation
. et joins à ma requête
. un modeste C.V.
. qui plaidera peut-être
. (je vous en serais gré)
. pour mon dossier.

.
. J’espère recevoir
. de votre main aimable
. dont je serais fort gré
. un avis favorable
. sur mon dossier.

.
. Dans l’attente éperdue
. de la lettre retour
. je vous reste dévoué
. et vous prie d’accepter
. vous supplie d’agréer
. en toute humilité
. quelques salutations
. très distinguées.

.
. Cordialement,

# vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:.:
