# Trouvaille

Quand je me saisis du carnet poussiéreux, mystérieusement 
abandonné dans cet [obscur endroit][perdu], un feuillet s’en 
échappe. Ma frontale y lit les mots suivants, griffonnés à la 
hâte :

_À Pierrick Rival, Jacques Gélat et l’Atelier d’écriture de 
l’ENS Cachan._

Qui l’a laissé là ? Pourquoi ? Quand ? Intriguée, là, en pleine 
exploration, j’entame la lecture du carnet. Il n’est pas bien 
long et j’en viens vite à bout. Je n’ai pas plus de réponse.

Que faire maintenant ?
D’autres l’ont peut-être trouvé avant moi.
D’autres le trouveront peut-être après moi.
Qui suis-je pour le garder pour moi seule ?
Je replace soigneusement le carnet où je l’ai découvert.

<small>Source : [textes][git-textes], [site][git-site].</small>

[perdu]: https://perdu.com
[git-site]: https://gitlab.crans.org/mevel/mkdocs-blog
[git-textes]: https://gitlab.crans.org/mevel/textes

<!--

# J’ai trouvé…

… ce qui, en vertu de toutes les conventions connues, devrait être une page
d’accueil mais, si j’espérais y découvrir quelque explication, j’en fus pour mes
frais. Me voilà résolument [égaré][].

[égaré]: https://perdu.com
-->

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
