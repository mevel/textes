---
date: 2018-02
modified: 2022-08-12
summary: 
    un texte sans chiffre romain (I, V, X, L, C, D),
    jeu d’écriture proposé par mes soins dans la Sauce,
    le journal étudiant de l’ENS Cachan
    (<a href="https://gitlab.crans.org/mevel/dictionnaires/-/blob/master/SIMPLIFICATION-ORTHOGRAPHE.md#chiffres-romains"
      >pourquoi je n’aime pas les chiffres romains</a>)
...

# Lipogramme latin

. À Joseph Arthème Fayart
. 31 rue Beaune
.
. 14 septembre.

Mes hommages,

J’enrage. On m’a refusé ma prose. J’eus beau essayer, pas moyen.

Ma prose ? Un roman. Son thème : affronter ses peurs. Son
héros : une sphère. Son passé : un mystère, un trauma tabou.
Personnage surprenant ! Pourtant, surmonté un juste étonnement,
on ne peut qu’être happé par sa quête amoureuse.
<br/>
Or, mon roman a été jugé « fort étrange et pas très beau. » Que 
non ! Sa beauté, j’y bosse sans arrêt. Sa forme est mesurée, son 
orthographe retenue : sur mon honneur, fautes et nombres en 
usage à Rome en sont absents. Bref : un joyau brut, genre
Parnasse.
<br/>
Y aura-t-on prêté une tare ou une autre ? Aura-t-on été ennuyé 
par son sujet ? Gêné par son ton un peu osé ? N’y aura-t-on pas 
goûté un humour somme toute assez gras ?

Bah ! Je supporte pour un temps un anonymat où je reste
embourbée. Autant pour mon ego frustré, passons. Nonobstant,
qu’on ne s’y trompe pas ! Je me targue que mon art surpasse tous
tes perroquets — seras-tu assez bête pour en penser
autrement ? — et ne restera pas toujours transparent. En tant
qu’auteure engagée, mon message, ma pensée, ne seront pas tus.

Oh, on ne bafoue pas que ma propre personne ! En un jour, en
effet, tu oppresses trente auteurs, en même temps que tu refuses
ta porte à septante autres.
<br/>
Trente stars à quatre sous, nègres sous-payés pour suer ton
argent. Poèmes passés, sagas ressassées, épopées répétées,
romans arrogants, phrases étouffantes, hantent tes étagères
austères. Tous, ersatz usés, ratés, éhontés pour un auguste
parangon. Mots jaquetés sans âme et sans mesure, et même en
orthographe SMS : tant que tu te bâffres en sous sonnants, tout
est bon. Sangsue, seras-tu un jour repue ?
<br/>
Et, retoqués sur ton perron, septante poètes martyres, esthètes
au bon goût, au sang neuf, bannerets soutenant un art neuf et
que, par appât, par paresse, par peur même, tu tues en œuf.
N’as-tu pas honte ?

Penses-tu que Rome eût poussé en un jour ? À tout genre, faut
son prototype. Un jour, ma prose gagnera. Et tu regretteras
amèrement, tu t’en gnaqueras tes pognes. Prospère tant que tu
oses, ton futur est sombre : un orage s’amasse sur ta tête.
<br/>
On portera mon art à nues et mon nom se saura : « Ah ! Fameuse
auteure. Son art est beau. Mes romans préférés. » Or ton propre
nom, Fayart, ton nom mourra. Tes presses monstrueuses et tous
tes rotors retors s’arrêteront. Tes rayonnages roguement fermés
à mon art seront tout juste bons pour un feu. Sans personne pour
te regretter, tu sombreras.

~ Anonyme.

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
