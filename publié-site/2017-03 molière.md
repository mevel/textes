---
date: 2017-03
summary: 
    écrit pendant la (terrible) polémique de la 
    « clause Molière » et l’élection présidentielle de 2017
title: Tragique accident…
...

# Tragique accident de grammaire sur un chantier

C’est à 17 h ce vendredi, sur le toit d’une maison particulière 
en construction à Cotice (Haute‐Loire), que se noue le drame. 
Alors qu’il s’apprête à boucler sa semaine, Stéphane Balette, 47 
ans, couvreur, commet deux erreurs de conjugaison. « *Il a voulu 
dire : “Je vous vois mardi”* », raconte Marco, blessé par ce 
premier coup mais dont les jours ne sont pas en danger, « *mais 
il a dit : “Je vous voit mardi.” On n’a pas compris ce qui se 
passait. On pensait avoir mal entendu. Éric lui a demandé de 
répéter, et alors…* » La gorge serrée, Marco ne finit pas son 
récit. Selon le rapport de police, l’homme répond : « *J’ai prit 
mon lundi.* » Face à lui, Éric Marchand et Cristian Ionescu, 54 
et 29 ans, meurent sur‐le‐coup. Bilan : deux morts, un blessé 
grave. M Balette s’en sort indemne mais en état de choc. Il est 
mis en examen pour violences verbales et homicide involontaire.

Selon l’expert verbalistique, « *Il s’agit de conjugaisons à la 
mauvaise personne. Il parlait de lui‐même, il aurait donc dû 
conjuguer le verbe “voir” à la première personne du singulier, 
avec un ‐s : “je vous vois”. Mais il l’a conjugué avec un ‐t, 
qui indique la troisième personne du singulier. Le second coup 
est du même type. Les erreurs de ce genre sont particulièrement 
dangereuses car elles ne s’entendent pas. Dans le métier on 
parle de “fautes homophones”.* »

« *Je ne lui en veux pas* », explique Marco. « *C’est un 
accident, ça aurait pu arriver à nous. Le travail est dur et on 
était au bout de la semaine, ça se voyait qu’il était fatigué. 
Nous autres collègues, on aurait dû faire plus attention à 
lui.* » L’homme était connu des services de police pour des cas 
récurrents d’expression en état d’ivresse. En 2011, il avait 
comparu pour un solécisme prononcé devant une école primaire. 
« *Monsieur Balette n’a rien d’exceptionnel* », déclare Sophie 
Lavreau, psychologue du commissariat. « *C’est un profil très 
banal.* »

L’Association de défense de la langue française reprend la balle 
au bond. « *Aujourd’hui,* » affirme son porte‐parole Armand 
Lindt, « *l’usage du français n’est pas réglementé. N’importe 
qui peut s’en servir sans aucun contrôle. Les nombreux 
francophones amateurs représentent un risque pour eux‐mêmes et 
pour leur entourage, et les accidents sont hélas inévitables. Le 
drame d’hier vient nous le rappeler.* » Et le porte‐parole de 
remettre en avant la proposition de l’ADLF d’un permis de 
français « *contraignant* ».

L’Académie française, quant à elle, rappelle un rapport de 2014 
où elle jugeait : « *Ouvrir la langue de Molière à la population 
fut une erreur. Elle n’est pas un vulgaire outil de 
communication — son dessein était et demeure ailleurs — et son 
maniement dans des conditions acceptables requiert un 
savoir‐faire dont seuls les professionnels peuvent disposer. 
Laissons l’art aux artisans.* »

L’Amicale laïque des francophones amateurs se dit « *indignée* » 
par un tel argumentaire. Le puissant groupe d’intérêts — qui 
peut compter sur le soutien de membres aussi influents que le 
journaliste Bernard Pivot et la magnate des cahiers de vacances 
Laëtitia Hachette — invoque « *les droits des consommateurs* » 
et la « *liberté de langue* » ; l’ALFA interpelle les candidats 
de l’élection présidentielle afin que le futur locataire de 
l’Élysée fasse inscrire dans la Constitution ces principes selon 
elle « *vitaux dans une démocratie telle que la nôtre.* » Elle 
estime que les accidents comme ceux d’hier soir restent 
marginaux, et qu’une démocratisation plus large de la langue 
permettrait d’en éviter encore davantage.

Le ministère de l’Intérieur n’a pour l’instant émis aucun 
commentaire.

À deux mois des présidentielles, l’événement vient cristalliser 
les tensions autour du débat, houleux, sur la réglementation du 
français. Les familles des victimes, toutefois, souhaitent 
éviter toute récupération politique et demandent que leur deuil 
soit respecté.

# vim: set tw=64 et ts=2 fo+=taw:
