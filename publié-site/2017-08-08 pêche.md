---
date: 2017-08-08
summary: un conte improvisé sur IRC et presque pas retouché depuis
...

# Le conte de Pêche

    02:58 <Fistine> Fl4cKsT0rM: si vous deviez résumer
                    votre vie professionnelle en un fruit,
                    lequel serait-ce ?
    02:58 <Fl4cKsT0rM> Fistine, la pêche !
    03:01 <Fistine> Bien répondu, c'est vite dit, mais
                    ta réponse me convient
    03:02 <Fl4cKsT0rM> (dans la vraie vie je mange pas
                       de pêche)
    03:02 <Fistine> Oh ?
    03:02 <Fistine> Conte-nous donc cette épique épopée !

Il y avait, dans le Cantal, trois frères fruits : Pêche était 
l’aîné, Pomme le puîné, et Poire le benjamin. En vérité, le 
jeune Abricot était leur cadet à tous, mais personne ne le 
comptait jamais, tant il était maigrichon. Ses trois aînés le 
tenaient à l’écart et l’humiliaient sans cesse par leurs jeux et 
leurs comptines. Pêche, l’aîné, était tout particulièrement 
méchant car qu’un fruit si rabougri partage ses couleurs le 
mettait très en colère.

Au comble du malheur, Abricot s’enfuit du village. Mais personne 
ne le rechercha. Soulagés d’être enfin débarrassés de ce fruit 
si chétif et si dégueu, ses frères effacèrent toute trace de 
leur honteux parent.

Vingt ans plus tard, le roi du Cantal, un vieil ananas tout 
fripé, était mort, et Pêche qui était son héritier était devenu 
le nouveau roi, et Pomme et Poire ses fidèles conseillers, 
serviles en tout. Et le royaume était malheureux car Pêche était 
cruel, il profitait de sa beauté et de sa peau douce pour 
amadouer les paysans et les taxer deux fois plus que de juste. 
Et il levait sans cesse de nouveaux impôts pour financer ses 
guerres de conquête contre le royaume voisin de Fruirouge et le 
plateau de Fromage.

C’est alors qu’un émissaire fruirouge arriva au château de 
Pêche. Et il parla à Pêche, et voici ce qu’il dit : « Ô puissant 
Pêche, t’es peut‐être le plus fort, mais tes sujets sont 
malheureux, et ce sera ta perte. » « Et on est les plus beaux. » 
« Na. » Alors Pêche entra dans une colère noire et il fit 
écrabouiller l’émissaire fruirouge, et le jus rouge inonda la 
salle du trône. Et il jura de conquérir le royaume de Fruirouge 
et d’écraser pareillement tous les fruirouges, jusqu’au dernier.

Alors il écrasa ses paysans sous plus d’impôts. Alors les 
paysans furent de plus en plus désespérés, et ils ouvrirent les 
yeux malgré la beauté éblouissante de Pêche, et ils virent que 
la classe royale les exploitait. Et ils se révoltèrent, 
s’approprièrent leurs moyens de production et refusèrent de 
payer aux fruits du roi. Poire eut vent de l’insurrection. Et il 
apprit que les paysans insoumis étaient menés par un certain 
Brie le Bon, et qu’il prêchait des idées nouvelles, et qu’il 
était très méchant envers le roi. Alors il en informa le roi 
Pêche, et Pêche ordonna à son armée d’aller écraser les paysans 
et de tuer ce Brie le Bon. Alors l’armée du roi incendia les 
villages et attaqua les paysans. Maints furent tués ou asservis, 
mais quelques un prirent le maquis dans les monts. Parmi eux 
était Brie le Bon.

L’armée les pourchassa longtemps, mais les rebelles 
connaissaient mieux le pays et il était facile de s’y cacher. 
Brie le Bon avait remarqué que les patrouilles étaient toujours 
menées par Poire, alors que normalement c’était Pomme le 
commandant‐en‐chef des armées. Cela éveilla son soupçon et il 
envoya un brugnon espionner le château. L’espion revint trois 
jours plus tard et dit qu’il n’avait vu Pomme nulle part, et 
qu’on racontait dans le château qu’il était parti à l’étranger 
chercher une arme pour vaincre définitivement les rebelles.

En entendant cela, Brie le Bon décida d’envoyer des expéditions 
dans tous les pays voisins pour découvrir où était allé Pomme et 
ce qu’il y faisait. Il envoya des fruits au royaume de 
Fruirouge, au plateau de Fromage, et il prit lui‐même la tête du 
groupe qui se rendait au puy du Dôme. Au royaume de Fruirouge, 
les fruirouges se montrèrent gentils (car ils se savaient plus 
beaux et ils avaient pitié) mais il ne purent rien leur dire que 
« Non, nous n’avons pas vu Pomme. Non, nous n’avons pas vu 
Pomme. Non, nous n’avons pas vu Pomme. » Au plateau de Fromage, 
les altiers camemberts ne daignèrent pas leur adresser la parole 
et ils ne trouvèrent nulle trace de Pomme. Au début, 
l’expédition menée par Brie le Bon se heurta aux mêmes 
difficultés. Le puy du Dôme était peuplé de farouches cantalous 
peu enclins à causer avec les étrangers, et qui apparemment 
n’avaient rien vu.

Jusqu’au jour où Brie le Bon rencontra une vieille bique qui 
s’était coincé le sabot dans un ravin. La chèvre avait l’air si 
triste, et elle faisait pitié à voir, et elle risquait de mourir 
si elle ne se dégageait pas, alors Brie le Bon descendit au fond 
du ravin et tenta de la décoincer. Il tira et poussa, à hue et à 
dia, en ahanant, mais après plus d’une heure d’effort sous le 
cagnard, il réussit à déloger le sabot. Alors la chèvre dit :

— Je suis Biquette et je parle peu, mais comme tu m’as sauvée je 
  veux bien t’aider un peu.

Brie le Bon répondit :

— Je suis Brie le Bon et je cherche un étranger comme moi, qui 
  s’appelle Pomme.
— J’ai aperçu un fruit, mais je ne connais pas son nom. Il 
  ressemblait à une pomme.
— C’est lui !
— Alors celui que tu recherches est un bien méchant fruit et je 
  ne vois pas pourquoi tu voudrais le trouver. Il est passé sur 
  ce chemin tout comme toi, il y a trois jours, avec une 
  compagnie de fruits en armes. Comme j’étais déjà coincée à 
  l’époque, j’ai bêlé à l’aide, mais j’ai eu beau bêler, je n’ai 
  rien récolté au début, et puis à la fin j’ai récolté des jets 
  de pierres, alors je me suis tu et j’ai attendu.
— Oh, pauvre Biquette ! :-(
— S’il passait par là, c’était forcément pour trouver mon 
  maître. Je crains pour lui car ce Pomme est bien vilain. 
  D’ailleurs, d’habitude mon maître vient me tirer d’un pareil 
  mauvais pas, et je crains qu’il ne lui soit arrivé malheur. 
  Allons le voir.

Biquette et Brie le Bon suivirent le chemin jusqu’à une cabane 
au fond des bois. Elle était très bien cachée et Brie le Bon ne 
l’aurait jamais trouvée sans Biquette. « Maî-î-î-ître », bêla 
Biquette, mais aucune réponse ne sortit de la cabane, alors ils 
entrèrent. À l’intérieur, tout était en désordre et cassé.

---

_**Notes de l’auteur.** J’ai eu un peu la flemme d’écrire la fin 
vu qu’il était 04:46 et que ça intéressait pas grand‐monde, du 
coup la fin prévue en accéléré : Maître est un magicien de type 
vieux sage à barbe, il révèle que Pomme lui a piqué trois choses 
magiques très puissantes (chose = couronne, anneau, etc·), il en 
forge une 4e pour Brie le Bon afin qu’il les vainquîsse et lui 
dit qu’ensuite il devra les détruire dans le puy du Dôme, Brie 
retourne au château de Pêche, il affronte ses 3 frères — car on 
découvre, ô stupeur, que Brie n’était autre qu’Abricot — et les 
vainc après un combat désespéré, il ramène les 4 choses au puy 
du Dôme, là il est tenté par le Mal mais finit par les détruire, 
fin heureuse, il devient roi, il règne longtemps, ses sujets 
sont béats dans leurs champs._

_Biquette sert un peu à rien actuellement, du coup je me suis 
dit que Maître pourrait mourir de ses blessures après avoir 
forgé la 4e chose, du coup Biquette suit Brie, devient son 
meilleur pote, apporte une aide indispensable dans la bagarre 
finale (de type 
précipiter‐un‐méchant‐dans‐le‐vide‐à‐coup‐de‐sabots), et l’aide 
à choisir la Lumière au sommet du puy du Dôme._

_Les influences sont évidemment _[Pêche-pomme-poire][comptine]_ 
et _[Le Retour du roi du Cantal][hobbit]_. D’autres questions ?_

_**Le dictionnaire amusant.** On en apprend, des choses…_

1. _L’« aîné » signifie simplement le plus âgé d’une fratrie 
   (absolu) ou de plusieurs gens qu’on compare (relatif). 
   Évident._
2. _Le « puîné » signifie exactement le second plus âgé d’une 
   fratrie, juste après l’aîné. Clair et net._
3. _Le « benjamin » signifie précisément le plus jeune d’une 
   fratrie. Sans ambigüité._
4. _Le « cadet » signifie, le plus souvent, **A** le second plus 
   jeune d’une fratrie (juste avant le benjamin), **B** tous les 
   membres d’une fratrie sauf l’aîné (ce qui est une notion 
   utile pour les successions), ou **C** le plus jeune de deux 
   gens (par opposition à l’aîné) ; mais aussi, plus 
   précisément, **D** celui qui est encadré entre deux autres 
   gens (de façon analogue au sens A) ; et par extension du 
   sens C, **E** « le moindre », mais alors seulement au sens 
   figuré (« le cadet de mes soucis ») parce que sinon on 
   comprendrait plus rien vu que c’est déjà ce que veut dire le 
   benjamin, enfin ça se dit quand même mais il faudrait pas. 
   Fa. Stoche._

---

_Le nécessaire et classieux exergue de la fin pour conclure :_

. J’ai dit au long fruit d’or : Mais tu n’es qu'une poire !
.~ [Victor Hugo, _Les contemplations_][hugo]

[comptine]: http://comptines.tv/pomme_peche_poire_abricot
[hobbit]: https://fr.wikipedia.org/wiki/Le_Hobbit_:_Le_Retour_du_roi_du_Cantal
[hugo]: https://fr.wikisource.org/wiki/Page:Hugo_-_Les_Contemplations,_Nelson,_1856.djvu/40

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
