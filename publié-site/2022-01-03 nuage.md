---
date: 2022-01-03
summary: atelier d’écriture, thème « Dormir sur un nuage »
title: Le voleur d’hélium
...

À peine un début…

# Le voleur d’hélium

Lorsqu’il eut enfoncé la tête perforatrice à travers l’enveloppe 
enduite et que le Puceron commença à siphonner sa cargaison, 
immatérielle et précieuse, Vryn s’accorda une pause. Agrippé 
d’une main au cordage, il scruta les nuages. Évidemment, il 
n’aurait pas été de sortie si l’horizon avait été dégagé ; les 
Puceronniers n’opèrent que par gros temps. Pour tout panorama, 
Vryn devait se contenter de la brume impénétrable et grisâtre 
dont il faisait son morne manteau. C’est pourquoi il avait pris 
l’habitude de faire travailler son imagination. Il se figurait 
au loin, lorsque le ciel était clair, de grandes voilures 
blanches : les manoirs volants des multi‐millionnaires.

Cela faisait bien longtemps qu’il n’en avait plus vu de près. 
Poursuivant leur croisière mystérieuse — qui pouvait dire à quoi 
ces gens pouvaient s’occuper ? — et, peut-être avant tout, 
maintenant leurs distances d’avec l’humanité sale, ils 
n’apparaissaient que ponctuellement. Pour Vryn et ses 
semblables, ils se résumaient à de petites formes géométriques 
— disques, ellipses, triangles — traversant l’horizon.

Vryn vérifia distraitement la jauge de pression.

Les riches… D’après Noë, c’est par eux que tout avait commencé. 
L’urbanisme aérien. La société du ciel. Même avant de 
s’affranchir de la gravité, lui avait-il raconté, ils étaient 
déjà au-dessus des lois (ne l’avaient-ils pas toujours été ? 
Vryn aurait eu du mal à croire le contraire), au-dessus des 
gens. Il ne leur restait plus guère d’autre moyen de s’élever 
encore.

« Dormez sur un nuage », promettaient les affiches.

<!--------------------------------------------------------------

  lexique:
  - aéronef, aérostat (vs. aérodyne)
  - dirigeable souple / rigide (Zeppelin)
  - carène = enveloppe (en toile de coton tendue sur des 
    poutrelles)
  - ~ armature
  - anneaux transversaux séparant les ballonnets
  - ballonnets = tranches de carène, contiennent le gaz
  - soupapes (1 par ballonnet)
  - quille = partie inférieure de la carène
  - coursive
  - nacelle = cabine
  - nacelle motrice (supporte un moteur, une hélice)
  - empennage, gouvernail de profondeur / de direction
  - ballasts = lests
  - hélium = gaz de sustentation
  - hélices
  - haubanage/hauban de renforcement

  https://www.larousse.fr/encyclopedie/divers/dirigeable/43767
  https://www.lavionnaire.fr/AerostatDirig.php
  http://inter.action.free.fr/publications/zep.pdf

  Vryn
  Noë
  Ève, Évey, Évy
  Yvonne
  Véro
  Rêve
  Noyr
  Ryo
  Nyo
  Yo
  Aryo

-->

# vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—:
