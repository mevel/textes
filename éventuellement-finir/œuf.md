---
date: 2018-11
modified: 2023-07-21
summary: Le dernier haïku du maître (une vieille idée inachevée)
title: ŒUF
...

# ŒUF

. Œil du hibou sage
. Un frisson de joie sauvage
. Fin du déchiffrage

**Œ**uvre nimbée de mystère, les derniers vers d’**I**keda 
résistent aux nombreuses spéculations les concernant. **L**a 
vérité ne sera peut-être jamais connue, puisque le maître s’est 
éteint paisiblement après avoir couché ces mots sur la dernière 
ligne de son célèbre cahier.

**D**es circonstances romanesques de son écriture à l’aura 
quasi-légendaire qui entoure plus généralement son auteur, de 
son sens énigmatique à ses surprenantes transgressions de forme, 
tout concourt à faire de ce poème une icône, aussi souvent citée 
dans la rue qu’elle est étudiée dans les universités. **U**n 
seul fait tangible : la structure est bien celle du haïku, art 
que l’auteur a porté à son zénith.

**H**élas ! si c’était aussi simple, les tout derniers mots 
d’**I**keda n’auraient peut-être pas éclipsé à la postérité les 
quelque cent-mille haïkus irréprochables qui lui sont attribués. 
**B**afouant une règle essentielle du genre, balayant 
négligemment l’œuvre d’une vie de lettres, l’ultime tirade ne 
contient pas de *kigo*, ces marqueurs de temporalité codifiés. 
**O**n ne peut que se perdre en conjectures sur ce manque 
crucial. **U**ne façon de tirer un trait sur sa vie ?

…

<!--------------------------------------------------------------

intro: circonstances romanesques, auteur iconique, incongrüité

transgression majeure = pas de kigo

hypothèses:
  - pas un haïku, dernières paroles non poétiques ?
  - marquer l’achèvement de sa vie ?
  - "Un frisson de joie sauvage" ~ "Un frisson, des oies sauvages"
    (rappelle un texte précédent de l’auteur) = en fait,  
    2 kigos pour l’automne !
    => dernier et plus grand tour du maître: texte 
    multidimensionnel (superposition), nous enseigne que les 
    mots peuvent être lus (suggérés) sans avoir été écrits 

le hibou:
  - un kigo quand même, évoquant la nuit profonde ?
  - symbolique: mystères nocturnes, peut-être inquiétant voire 
    effrayant (cf nouvelles fantastiques)
    associé à la mort (cf mort de l’auteur)
    mais aussi au savoir/science/sagesse (cf champ lexical)
    => il s’agit ici de percer un secret grâce à l’observation 
    et à la science [transition !]

"fin du déchiffrage" = ?
  - déchiffrage de quoi ?
      + le poète se voit-il comme un prophète qui lit le grand 
        livre de la Nature et en traduit des fragments dans la 
        langue des mortels ?
      + au contraire, est-ce son œuvre qui est chiffrée et son 
        lecteur qui travaille dur pour la décoder ?
  - fin = la mort ? ou simplement la conclusion de son œuvre, 
    achevée au cœur de la nuit (le hibou) dans une ultime 
    inspiration (le "frisson" serait alors l’excitation d’avoir 
    conclu enfin son œuvre ?)
    => autre interprétation possible: travail nocturne emporté 
    jusqu’au petit matin par l’inspiration / passion (écrivain 
    ou lecteur)

une question vient: si ce haïku est la fin, quel est le début? 
jusqu’où remonter? de nombreux critiques ont fait le lien avec 
le 1er haïku qu’on lui connaisse (et dont on pensait auparavant 
avoir tout compris); faire revenir le lecteur aussi loin en 
arrière et lui faire comprendre qu’un nouveau sens s’y cachait, 
que le moindre mot était calculé depuis le début, restera comme 
le plus formidable tour de force du maître.

    . Œil ouvert, un cri
    . ...
    . Début du printemps

    (début/fin, printemps/automne, jeunesse/mort, jour/nuit)

----------------------------------------------------------------

codage stéganographique itéré:

  - majuscule précédée par une consonne -> minuscule
  - majuscule précédée par une voyelle -> majuscule
  - nouveau paragraphe -> espace
  - saut de paragraphe -> nouveau paragraphe
  - nouveau chapitre -> saut de paragraphe
  - nouveau tome -> nouveau chapitre
  - ...
  - la ponctuation est codée par des séquences de lettres, p.ex.
        "poi" -> "."    "vir" -> ","
        "exc" -> "!"    "dém" -> ":"
        "int" -> "?"    etc.
        "sus" -> "…"

pour une mise en abyme la plus parfaite possible: le codage 
inclut le paratexte (titres de chapitres, titre du livre, 
exergues, mentions d’impression, etc.)

bonus: on peut cacher plusieurs choses avec des codages 
parallèles ; par exemple, une image serait décrite par ses 
pixels avec les occurrences des mots "noir" et "blanc".

-->

<!-- vim: set tw=64 et ts=2 sw=0 fo+=taw com+=fb\:—: -->
